# Fassster Project

Api documentation for fassster

## Getting Started

git clone https://gitlab.com/jokerxvier/fassster-api.git

### Prerequisites

What things you need to install the software and how to install them

```
 * PHP >= 7.0.0
 * LARAVEL >= 5.5.*
 * Ubuntu >= 16.04
 * mdbtools
 
```

### Step 1 :  Installing
Clone the fassster repository

```
git clone https://gitlab.com/jokerxvier/fassster-api.git
```

And Go to the fassster project directory

```
cd fassster-api
run composer install
```

## Step 2 : Generate Application Key

The next thing you should do after installing Laravel is set your application key to a random string.

```
php artisan key:generate
```

## Step 3 : Create folders 
Create a folders below. This is required when uploading zip file and mdb
```
storage/mdb
public/projects/zip
```

## Step 3 :  Give Directory Permission

Directories should be writable by your web server or Project will not run

```
* storage
* bootstrap/cache
* storage/mdb
* public/projects/zip
```

## Step 4 : Install Dependencies

Below are the dependencies needed to run the project

```
sudo apt-get install mdbtools
```

## Step 5 : Env configuration

```
cp .env-example .env
```
Edit .env in the fassster-project

### Set up database connection
Edit .env and fill up credentials below to connect with the database
```
DB_CONNECTION=mysql
DB_HOST=[Server/Host]
DB_PORT=3306
DB_DATABASE=[Database Name]
DB_USERNAME=[Database User Name]
DB_PASSWORD=[Database Password]
```

## Step 6:   Generate database migrations
To Generate database tables and migrations

```
php artisan migrate
```
### Generate Database migration with test data [Optional]
To Generate database tables and migrations with test data

```
php artisan migrate:refresh --seed
```

## Step 7:   Set up Cron job for uploading and parsing stem project
Starting The Scheduler you only need to add the following Cron entry to your server. 

```
* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
```

This Cron will call the Laravel command scheduler every minute. When the schedule:run command is executed, Laravel will evaluate your scheduled tasks and runs the tasks that are due.

## Directory Structure

### routes
The routes directory contains all of the route definitions for your application. By default, several route files are included with Laravel: web.php, api.php, console.php and  channels.php. All the api endpoints are stored in api.php

    .
        ├── ...
        ├── routes                    
        │   ├── api.php          # This is where all the api endpoints are declared
        └── ...

### The App/Http Directory
The Http directory contains your controllers, middleware, and form requests. Almost all of the logic to handle requests entering your application will be placed in this directory.

    .
        ├── ...
        ├── Http                   
        │   ├── Controllers        # This is the directory that holds your controllers.
        │   ├── Middleware         # This is the folder you will store your Middleware 

### Http/Controllers/Api Directory 
This is the directory that holds your controllers that contain logic for interacting with models and rendering appropriate views to the client.

    ├── ...
        ├── Api                  
        │   ├── Dashboard       # Contains all the logic in dashboard 
        │   ├── Mdb             # Contains all the logic related to mdb including upload and generating reports
        │   ├── Projects        # Contains all the logic related to projects and upload zip projects
        │   ├── Scenario        # Contains all the logic related to scenario 
        │   ├── Regions         # Display all the Regions
        │   ├── Roles           # Display all the User roles
        │   ├── Search          # Contains all the logic in searching for users and scenario
        │   ├── Simulations     # Contains all the logic in getting the simulation result from the stem project 
        │   ├── User            # Contains all the logic for users


## Assets and Upload Files/Zip Directory

### public/projects/images
This is the directory that holds all the  avatar for stem project

### public/projects/zip
This is the directory that holds all the  uploaded stem projects

### storage/mdb 
This is the directory that holds all the  uploaded mdb files



## Authors

* **Jason Javier** - Backend Developer
* **Email** : jasonjavier06@gmail.com
