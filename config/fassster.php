<?php


return [

    'image' => [
        'width' => 400,
        'height' => 400
    ],

    'upload' => [
        'project' => 'projects/images',
        'zip' => 'projects/zip',
        'scenario' => [
            'zip' => 'scenario/zip',
            'extracted' => 'scenario/extracted' 
        ],
        'mdb' => storage_path('mdb')
    ],

    'image_src' => env('APP_URL') .  '/images/original',

];
