<?php

namespace App\Http\Controllers\Api\Scenario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\User;
use App\Models\Scenarios;
use App\Models\Statistics;
use App\Models\Projects;
use JWTAuth, DB, File, Config, Zip;


class ScenarioController extends ApiController
{
	protected $scenarios;
	protected $limit = 10;

    public function __construct(Scenarios $scenarios)
    {
    	$this->scenarios = $scenarios;

    }

    public function index(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

		$rules = [
    		'project_id' => 'required|exists:projects,id',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

		$scenarios = $this->scenarios
					->where('project_id', $request->project_id)
					->whereHas('users', function($q) use($user){
			 			$q->where('user_id', $user->id);
					});

        if ($request->has('status')) {
            $scenarios = $this->scenarios->where('status', $request->status);
        }

        $scenarios =   $scenarios->orderby(DB::raw('case when type= "main" then 1 when type= "sub" then 2 end'));

		
		$scenarios = $scenarios->paginate($this->limit);

		return $this->json(['data' => $scenarios, 'success' => true]);
    }


    public function getScenario(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }


        $rules = [
            'project_id' => 'required|exists:projects,id',
        ];

        $validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
        if ($validate) { return $validate; }



        $scenarios = $this->scenarios
                    ->select('id', 'title', 'status')
                    ->where('project_id', $request->project_id)
                    ->whereHas('users', function($q) use($user){
                        $q->where('user_id', $user->id);
                    })->orderby(DB::raw('case when type= "main" then 1 when type= "sub" then 2 end'))->get()->makeHidden(['owner', 'members', 'ownership_type', 'parameters_data', 'decorators_data']);

        return $this->json(['data' => $scenarios, 'success' => true]);
    }

    public function show(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $rules = [
            'scenario_id' => 'required|exists:scenarios,id',
        ];

        $validate = $this->validateRequest($request->all(), $rules);

        //validate data
        if ($validate) { return $validate; }

        $scenario = $this->scenarios::find($request->scenario_id);

        return $this->json(['data' => $scenario, 'success' => true]);

    }

    public function getByStatus(Request $request) {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $rules = [
            'status' => 'required',
        ];

        $validate = $this->validateRequest($request->all(), $rules);



        //validate data
        if ($validate) { return $validate; }

        $scenarios = $this->scenarios
                    ->select('id', 'title', 'status')
                    ->where('status', $request->status)
                    ->whereHas('users', function($q) use($user){
                        $q->where('user_id', $user->id);
                    })->paginate($this->limit)
                    ->makeHidden(['owner', 'members', 'ownership_type', 'parameters_data', 'decorators_data']);
        
        return $this->json(['data' => $scenarios, 'success' => true]);

    }

    public function create(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

		$rules = [
    		'project_id' => 'required|exists:projects,id',
    		'title' => 'required|max:200',
            'created' => 'required|date_format:"Y-m-d"',
    		'description' => 'sometimes|max:500',
    		'parameters' => 'required|array',
    		'decorators' => 'required|array',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

        $project = Projects::find($request->project_id);

    	$scenario = $this->scenarios::create([
    					'title' =>  $request->title,
    					'description' => $request->description,
    					'project_id' => $request->project_id,
                        'type' => 'sub',
                        'owner' => $user->id,
                        'created' => $request->created
    				]);

        $main_scenario = $this->scenarios->where('project_id', $request->project_id)->where('type', 'main')->first();

        if ($project->zip_path)
        {
           
            if (File::exists(public_path($project->zip_path)))
            {  
                $destination = Config::get('fassster.upload.scenario.zip') . '/' .  $scenario->id . '_' . time() . '.zip';
                File::copy(public_path($project->zip_path), public_path($destination));

                $scenario->zip_path = $destination;
                $scenario->save();
            }

        }


    	$this->insertParameters($request, $scenario)
    		->insertDecorators($request, $scenario, $main_scenario)
    		->attachOwnership($user, $scenario);

    	return  $this->json(['success' => true, 'data' => $scenario]);
    }

    public function duplicate(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

    	$rules = [
            'scenario_id' => 'required|exists:scenarios,id'
        ];


        $validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
        if ($validate) { return $validate; }

        $scenario = $this->scenarios::find($request->scenario_id);

        if (!$scenario) { 
            return $this->json_error(['success' => false,'message' => 'not_found']);  
        }



   
        $clone = Scenarios::create([
        	'title' => 'Copy ' .  $scenario->title,
        	'description' => $scenario->description,
        	'project_id' => $scenario->project_id,
        	'type' => 'sub',
        	'owner' => $user->id,
            'created' => $scenario->created
       ]);

        if ($scenario->zip_path)
        {
          

            if (File::exists(public_path($scenario->zip_path)))
            {  
                $destination = Config::get('fassster.upload.scenario.zip') . '/' .  $clone->id . '_' . time() . '.zip';
                File::copy(public_path($scenario->zip_path), public_path($destination));

                $clone->zip_path = $destination;
                $clone->save();

            }

           
        }
       
        if ($scenario->statistics->count() > 0)
        {
        	foreach ($scenario->statistics as $stat)
        	{

        		Statistics::create([
					'scenario_id' => $clone->id,
					'stat_value' => $stat->stat_value,
					'stat_key' => $stat->stat_key,
                    'created' => $stat->created,
                    'targetISOKey' => $stat->targetISOKey,
					'type' =>  $stat->type,
                    'created' => $stat->created
				]);

        	}
        }
	

		$updatedClone = Scenarios::find($clone->id);
	

        $this->attachOwnership($user, $clone);

        return  $this->json(['success' => true, 'data' => $updatedClone]);

        

    }

    private function attachOwnership($user, $scenario)
    {
    	$scenario->owner()->attach($user->id, ['type' => 'owner']);

    	return $this;

    }


    private function insertParameters($request, $scenario)
    {
    	
    	if ($request->has('parameters') && count($request->parameters) > 0)
		{

			foreach ($request->parameters as $key =>  $value)
			{
				$data = [
					'stat_key' => $key, 
					'stat_value' => $value, 
					'type' => 'parameter', 
					'scenario_id' => $scenario->id,
                    'created' => $request->created
				];

				Statistics::firstOrCreate($data);

				
			}

		}

		return $this;
    }

    private function insertDecorators($request, $scenario, $main_scenario)
    {
    	
    	if ($request->has('decorators') && count($request->decorators) > 0)
		{

			foreach ($request->decorators as $key => $value)
			{
                $stat = Statistics::where('scenario_id', $main_scenario->id)->where('stat_key', $key)->first();
				$data = [
					'stat_key' => $key, 
					'stat_value' => $value, 
					'type' => 'decorator', 
					'scenario_id' => $scenario->id,
                    'targetISOKey' => $stat->targetISOKey,
                    'created' => $request->created
				];

				Statistics::firstOrCreate($data);
			}

		}

		return $this;
    }



    public function update(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

		$rules = [
    		'scenario_id' => 'required|exists:scenarios,id',
    		'title' => 'required|max:200',
    		'parameters' => 'sometimes|array',
    		'decorators' => 'sometimes|array',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);


        
        //validate data
    	if ($validate) { return $validate; }

		$scenarios = $this->scenarios
					->where('id', $request->scenario_id)
					->whereHas('users', function($q) use($user){
			 			$q->where('user_id', $user->id);
					});

		if ($scenarios->count() <= 0)
		{
			return $this->json_error(['success' => false,'message' => 'restricted_page']);
		}

        $scenario = $scenarios->first();

        $scenario->title = $request->title;
        $scenario->description = $request->description;
        $scenario->save();

        $this->updateDecorators($request, $scenario)
                    ->updateParameters($request, $scenario);

        $zip_path = $scenario->zip_path;

        if (File::exists(public_path($zip_path)))
        {
            $filename = $scenario->id . '_' . time();
            $extracted_path = Config::get('fassster.upload.scenario.extracted') . '/' . $filename;
            $destination  = Config::get('fassster.upload.scenario.zip') . '/' . $filename . '.zip';

            $this->extract($zip_path, $extracted_path);

            $files = $this->scenarios->findAllFiles($extracted_path);

            $this->scenarios->updateXmlDecorators($files, $scenario)
                ->updateXmlParameters($files, $scenario)
                ->zip($extracted_path, $destination);

            //Delete extracted path
            //delete scenario zip path and replace with the new once
            
            if (file_exists(public_path($extracted_path)))
            {
                File::deleteDirectory(public_path($extracted_path));
            }    

            if (file_exists(public_path($scenario->zip_path)))
            {
                File::delete(public_path($scenario->zip_path));
            }

            

            $scenario->zip_path = $destination;
            $scenario->status = 'pending';
            $scenario->save();

        }


        return  $this->json(['success' => true, 'data' => $scenario]);

    }

    private function updateDecorators($request, $scenario)
    {


		if ($request->has('decorators') && count($request->decorators) > 0)
		{

			foreach ($request->decorators as $key =>  $value)
			{

				Statistics::where('scenario_id', $scenario->id)
					->where('stat_key', $key)
					->where('type', 'decorator')
					->update(array('stat_value' => $value));
			}

		}

	

		return $this;

    }

    private function updateParameters($request, $scenario)
    {


		if ($request->has('parameters') && count($request->parameters) > 0)
		{

			foreach ($request->parameters as $key =>  $value)
			{

				Statistics::where('scenario_id', $scenario->id)
					->where('stat_key', $key)
					->where('type', 'parameter')
					->update(array('stat_value' => $value));
			}

		}

	

		return $this;

    }

    public function delete(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

    	$rules = [
            'scenario_id' => 'required|exists:scenarios,id'
        ];


        $validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
        if ($validate) { return $validate; }

        /*$scenario = $this->scenarios::whereHas('users', function ($q) use($user) {
            $q->where('user_id', $user->id);
        })->where('id', $request->scenario_id);*/

        $scenario = $this->scenarios::where('id', $request->scenario_id);

        if ($scenario->count() <= 0) { 
            return $this->json_error(['success' => false,'message' => 'not_found']);  
        }

        $scenario->delete();

        return $this->json(['message' => 'delete_success', 'success' => true]);

    }

    private function extract($zip_path, $extracted_path) 
    {
  
        $zipFile = Zip::open(public_path($zip_path));

        $zipFile->extract(public_path($extracted_path));

        chmod ($extracted_path, 0777);

        return $this;
    }

    
}

