<?php

namespace App\Http\Controllers\Api\Scenario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Scenarios;
use App\Models\Members;
use App\User;
use JWTAuth;

class ShareController extends ApiController
{
    protected $scenarios;

    public function __construct(Scenarios $scenarios)
    {
    	$this->scenarios = $scenarios;
    }

   
    public function scenario(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$rules = [
    		'scenario_id' => 'required|exists:scenarios,id',
        	'user_ids' => 'required|array', 
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	
    	$scenario_id = $request->has('scenario_id') ? $request->scenario_id : false;
    	$user_ids = $request->has('user_ids') ? $request->user_ids : false;

    	$user_collection = array_filter($user_ids, function($key) use ($user) {
		    return ($key != $user->id);
		});


    	$users = User::whereIn('id', $user_collection)->pluck('id')->toArray();
    	$scenarios = $this->scenarios::find($scenario_id);

    	if (!$scenarios) { return $this->json_error(['success' => false,'message' => 'restricted_page']);  }

    	if (count($users) > 0)
    	{
    		$this->AddToProjectMembers($users, $scenarios);
    	}

    	return $this->json(['data' => $scenarios->members, 'success' => true]);
    }

    public function remove_project_members(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$rules = [
    		'scenario_id' => 'required|exists:scenarios,id',
        	'user_id' => 'required|exists:users,id', 
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	$members = Members::where('user_id', $request->user_id)->where('scenario_id', $request->scenario_id);

    	if ($members->count() <= 0)
    	{
    		return $this->json_error(['success' => false,'message' => 'user_not_found']); 
    	}

    	$members->delete();

    	$scenarios = $this->scenarios::find($request->scenario_id);


    	return $this->json(['data' => $project->members, 'success' => true]);


    }


    private function AddToProjectMembers($users, $scenario)
   	{
   		foreach($users as $user)
   		{
   			$scenario->members()->syncWithoutDetaching([$user => ['type' => 'member']]);
   		}

   	}
}
