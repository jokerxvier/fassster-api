<?php

namespace App\Http\Controllers\Api\Region;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Region;

class RegionController extends ApiController
{
    public function index()
	{
		$region = Region::all(['id', 'name']);

		return $this->json(['data' => $region, 'success' => true]);
	}
}
