<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ApiController extends Controller
{
    protected $success_code = 201;
	protected $error_code = 401;

    public function json($data, $headers = [])
    {
        return response()->json($data, $this->success_code, $headers);
    }


    public function json_error($data, $headers = [])
    {
        return response()->json($data, $this->error_code, $headers);
    }

    

    public function validateRequest($params, $rules)
    {

        $validator = Validator::make($params, $rules);

        if($validator->fails()) {
            $err = [
                'success' => false,
                'message' => $validator->errors()->all()
            ];

            return $this->json_error($err);
        }else {

            return false;
        }

    }
}
