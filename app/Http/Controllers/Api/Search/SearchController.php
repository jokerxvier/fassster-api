<?php

namespace App\Http\Controllers\Api\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Scenarios;

class SearchController extends ApiController
{
    public function __construct()
    {

    }

    public function users(Request $request)
    {
    	if ($request->has('search') && $request->search != '') 
    	{

    		$users = User::search($request->search)->take(20)->get();

    		return $this->json(['data' => $users, 'success' => true]);
    	}

    	return $this->json(['message' => 'not_found', 'success' => false]);

    }

    public function scenario(Request $request)
    {
        if ($request->has('search') && $request->search != '') 
        {

            $data = Scenarios::search($request->search)->take(20)->get();

            return $this->json(['data' => $data, 'success' => true]);
        }

        return $this->json(['message' => 'not_found', 'success' => false]);

    }
}
