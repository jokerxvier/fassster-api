<?php

namespace App\Http\Controllers\Api\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Projects;
use JWTAuth, Carbon\Carbon, DB;

class DashboardController extends ApiController
{
	protected $date;

    public function __construct()
    {


    }

    public function index()
    {

    	$arr = [];
    	$arr['disease'] = [];
    	$arr['projects'] = [];
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

		$allProjects = Projects::select(DB::raw('count(*) as total'))->whereHas('users', function($q) use($user){
			 				$q->where('user_id', $user->id);
						})->first();

		$uploadedProjects = Projects::select(DB::raw('count(*) as total'))->whereHas('owner', function($q) use($user){
			 				$q->where('user_id', $user->id);
						})->first();
		$sharedProjects = Projects::select(DB::raw('count(*) as total'))->whereHas('members', function($q) use($user){
			 				$q->where('user_id', $user->id);
						})->first();

		$lastSevenDays = Projects::select(DB::raw('count(*) as total'))
					->whereHas('users', function($q) use($user){
			 			$q->where('user_id', $user->id);
					})
					->where('created_at', '>=', Carbon::now()->subDays(7)->toDateTimeString())
					->where('created_at', '<=', Carbon::now()->toDateTimeString())
					->first();


		$lastSixMonths = Projects::select(DB::raw('count(*) as total'))
					->whereHas('users', function($q) use($user){
			 			$q->where('user_id', $user->id);
					})
					->where('created_at', '>=', Carbon::now()->subMonths(6)->toDateTimeString())
					->where('created_at', '<=', Carbon::now()->toDateTimeString())
					->first();



		$top_disease = $this->disease($user)
					->pluck('total', 'disease_name')->toArray();

		$others_disease = Projects::select(DB::raw('count(*) as total'))
					->where('created_at', '>=', Carbon::now()->subMonths(6)->toDateTimeString())
					->where('created_at', '<=', Carbon::now()->toDateTimeString())
					->whereHas('users', function($q) use($user){
			 				$q->where('user_id', $user->id);
					})
					->orderBy('total', 'DESC')
					->whereNotIn('disease_name', $this->disease($user)->pluck('disease_name')->toArray())
					->first();


		$arr['projects']['last_seven_days'] = $lastSevenDays->total;
		$arr['projects']['last_six_months'] = $lastSixMonths->total;
		$arr['projects']['all_count'] = $allProjects->total;
		$arr['projects']['uploaded_count'] = $uploadedProjects->total;
		$arr['projects']['shared_count'] = $sharedProjects->total;

		if (count($top_disease) > 0)
		{
			$arr['disease'] = $top_disease;
			$arr['disease'] = array_merge($arr['disease'], ['others' => $others_disease->total]);
		}
			

		return $this->json(['data' => $arr, 'success' => true]);
    }

    private function disease($user)
    {
    	return Projects::select(DB::raw('count(disease_name) as total'), 'projects.disease_name')
    				->whereHas('users', function($q) use($user){
			 				$q->where('user_id', $user->id);
					})
					->where('created_at', '>=', Carbon::now()->subMonths(6)->toDateTimeString())
					->where('created_at', '<=', Carbon::now()->toDateTimeString())
					->groupBy('disease_name')
					->orderBy('total', 'DESC')
					->take(4);
    }
}
