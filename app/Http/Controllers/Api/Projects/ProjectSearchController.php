<?php

namespace App\Http\Controllers\Api\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Projects;

class ProjectSearchController extends ApiController
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
    	if ($request->has('search') && $request->search != '') 
    	{

    		$data = Projects::search($request->search)->take(20)->get();

    		return $this->json(['data' => $data, 'success' => true]);
    	}

    	return $this->json(['message' => 'not_found', 'success' => false]);

    }
}
