<?php

namespace App\Http\Controllers\Api\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Projects;
use App\Models\Scenarios;
use App\Models\Simulations;
use JWTAuth, Image, Config;


class ProjectController extends ApiController
{
	protected $projects; 
	protected $limit = 10;
	protected $destination_path; 

    public function __construct(Projects $projects)
    {
    	$this->projects = $projects;
    	$this->destination_path = Config::get('fassster.upload.project');
    }

    public function index()
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


		$project = $this->projects
					->whereHas('users', function($q) use($user){
			 			$q->where('user_id', $user->id);
					})->paginate($this->limit);


		return $this->json(['data' => $project, 'success' => true]);
    }


    public function getProject()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }


        $project = $this->projects
                    ->whereHas('users', function($q) use($user){
                        $q->where('user_id', $user->id);
                    })->pluck('title', 'id');


        return $this->json(['data' => $project, 'success' => true]);
    }

    public function show(Projects $projects, Request $request)
    {

    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$project_id = $request->has('project_id') ? $request->project_id : false;

    	$project = $this->projects::whereHas('users', function ($q) use($user) {
    		$q->where('user_id', $user->id);
    	})->where('id', $project_id)->first();

    	if (!$project) { return $this->json_error(['success' => false,'message' => 'restricted_page']);  }

    	return $this->json(['data' => $project, 'success' => true]);

    }

    public function edit(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}



		$rules = array_add($this->projects::$rules, 'project_id', 'required|exists:projects,id');

		$validate = $this->validateRequest($request->all(), $rules);

    	if ($validate) {
    		return $validate;
    	}

		$project_id = $request->has('project_id') ? $request->project_id : false;
		$image = '';

		$project = $this->projects::whereHas('users', function ($q) use($user) {
    		$q->where('user_id', $user->id);
    	})->where('id', $project_id)->first();

    	
    	if (!$project) { return $this->json_error(['success' => false,'message' => 'restricted_page']);  }

    	if ($request->has('image'))
    	{

    		$image = $this->uploadImage($request, $project);

    		$project->image = $image;

    	}

    	$project->title = $request->title;
    	$project->description = $request->description;
    	$project->save();

    	$updated_data  = $this->projects::find($project_id);

    	return $this->json(['data' => $updated_data, 'success' => true]);



    }

    private function uploadImage($request, $project)
    {
    	$image = $request->file('image');
        $image_name = $project->id . '_' . time().'.'.$image->getClientOriginalExtension();

      
        $destinationPath = public_path($this->destination_path .'/'. $image_name);

        $img = Image::make($image->getRealPath());

        $img->resize(Config::get('fassster.image.width'), Config::get('fassster.image.height'), function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath);

		if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }

		return $image_name;
    }

    public function delete(Request $request)
    {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

    	$rules = [
            'project_id' => 'required|exists:projects,id'
        ];

        $validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
        if ($validate) { return $validate; }

        $project = $this->projects::whereHas('users', function ($q) use($user) {
            $q->where('user_id', $user->id);
        })->where('id', $request->project_id);

        if ($project->count() <= 0) { 
            return $this->json_error(['success' => false,'message' => 'not_found']);  
        }

        $project->delete();

        return $this->json(['message' => 'delete_success', 'success' => true]);


    }

    public function test() {
        /*$target_url = 'http://202.125.102.201/fassster-stem/public/api/stem/upload';
        Scenarios::where('status', 'pending')
          ->chunk(100, function ($scenarios) use($target_url){

            foreach($scenarios as $scenario)
            {
              $file_path = public_path($scenario->zip_path);

              $cfile = new \CurlFile($file_path,'application/zip', $scenario->title);

              $post = array('project_id' => $scenario->id,'project'=> $cfile);
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL,$target_url);
              curl_setopt($ch, CURLOPT_POST,1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
              $result=curl_exec ($ch);
              $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
              curl_close ($ch);

              if ($httpCode == 200) {
                $res =  json_decode($result, true);
                if (array_key_exists('success', $res)) {
                  if ($res['success'] == true) {
                    $scenario->status = 'queued';
                    $scenario->save();
                  }
                }
              }

              return;
            }
          });*/

          $path = 'scenario/extracted/2_1524155585/DenguePhilippinesGeneric/decorators/Altavas.standard';

          $decorator = simplexml_load_file(public_path($path));
          $targetISOKey = (string) $decorator->attributes()->targetISOKey;

          $decorator->attributes()->infectiousCount = 1000;
          $res = $decorator->asXML(public_path($path));

          dd($res);
    }

    public function getQueuedScenario() {
      Scenarios::where('status', 'queued')
          ->chunk(100, function ($scenarios){
            foreach($scenarios as $scenario)
            {
              $url = public_path('simulation.json');
              $json = file_get_contents($url);
              $res = json_decode($json, true);
   
              if (array_key_exists('data', $res)) {
                 $scenario->statistics()->delete();
                 Simulations::insertSimulation($res['data'], $scenario->id);

                 $scenario->status = 'active';
                 $scenario->save();
              }
           
              return;
            }
          });
    }
}
