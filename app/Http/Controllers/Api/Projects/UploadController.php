<?php

namespace App\Http\Controllers\Api\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Models\Projects;
use App\Models\Scenarios;
use App\Models\Statistics;
use JWTAuth, Config, Zip, File, XmlParser, Carbon\Carbon;

class UploadController extends ApiController
{

	protected $destination_path;
	protected $project;

    public function __construct(Projects  $project)
    {
    	$this->destination_path = Config::get('fassster.upload.zip');
    	$this->project = $project;
    }

    public function project(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$rules = [
    		'zip' => 'required|mimes:zip,7z,rar',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }



    	if ($request->hasFile('zip')) 
    	{
    
	    	$zip = $request->file('zip');

	    	$name = $user->id . '_' . time();

	    	$filename = $name  .'.'.$zip->getClientOriginalExtension();

	    	$uncompressed_dir = $this->destination_path . '/' . $user->id . '/' . time();

	    	

	    	$project_title = basename($zip->getClientOriginalName(), ".zip");



	    	$this->moveFile($zip, $filename)
	    		->extract($filename, $uncompressed_dir);

	    	//$files = $this->findAllFiles(public_path($uncompressed_dir));

           $files = $this->findScenario(public_path($uncompressed_dir));

           $data = $this->parseData($files);


            if (count($files) <= 0)
            {
                

                File::deleteDirectory(public_path($uncompressed_dir));
                File::delete(public_path($this->destination_path . '/' .  $filename));

                return $this->json_error(['message' => 'invalid_zip', 'success' => false]);

            }
	    	  
            $zip_path = $this->destination_path . '/' . $filename;

	    	$project = $this->project::create(['title' => $project_title, 'zip_path' => $zip_path, 'disease_name' => $data['parameters']['data']['diseaseName'], 'created' => $data['parameters']['created']]);


	   		$this->attachProjectOwnership($user, $project)
                ->addScenario($user, $project, $data,  $filename);

	   		File::deleteDirectory(public_path($uncompressed_dir));

	   		return $this->json(['data' => $project, 'success' => true]);
	   	}

	   	return $this->json_error(['message' => 'invalid_zip', 'success' => false]);

    }



    private function addScenario($user, $project, $data, $filename)
    {
  
        

        $destination = Config::get('fassster.upload.scenario.zip') . '/' .  $filename;



        if (File::exists(public_path($project->zip_path)))
        {   
            File::copy(public_path($project->zip_path), public_path($destination));
        }
        
   

    	$scenario = Scenarios::create(['project_id' => $project->id, 'title' => $project->disease_name, 'type' => 'main', 'zip_path' => $destination, 'created' => $data['parameters']['created']]);

    	$this->attachScenarioOwnership($user, $scenario)->addStatistic($scenario, $data);



    	return $this;
    }


    private function attachProjectOwnership($user, $project)
    {
    	$project->owner()->attach($user->id, ['type' => 'owner']);

    	return $this;

    }


    private function attachScenarioOwnership($user, $scenario)
    {
    	$scenario->owner()->attach($user->id, ['type' => 'owner']);

    	return $this;

    }

    private function addStatistic($scenario, $data)
    {
        $parameters = [];
        $decorators = [];


        if (array_key_exists('diseaseName', $data['parameters']['data']))
        {
            unset($data['parameters']['data']['diseaseName']);
        }
     

        foreach ($data['parameters']['data'] as $key => $p)
        {

                $parameters[] = [

                    'stat_key' => $key,
                    'stat_value' => $p,
                    'type' => 'parameter',
                    'scenario_id' => $scenario->id,
                    'created' => $scenario->created,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ];
        }


        foreach ($data['decorators'] as $key => $d)
        {
      
                $decorators[] = [

                    'stat_key' => $d['stat_key'],
                    'stat_value' => $d['stat_value'],
                    'type' => 'decorator',
                    'targetISOKey' => $d['targetISOKey'],
                    'created' => $scenario->created,
                    'scenario_id' => $scenario->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ];
        }

       
    	
    	Statistics::insert($parameters);
    	Statistics::insert($decorators);

    	return $this;
    }
    //, $scenario
    private function parseData($files)
    {

    	if (count($files) > 0)
    	{
    		$result = [];



            if (array_key_exists('decorators', $files) && array_key_exists('parameters', $files))
            {

                foreach($files['parameters'] as $param_file)
                {
                     $params = simplexml_load_file($param_file);

                     $params = json_decode(json_encode((array)$params), TRUE);

                     $result['parameters']['created'] =  Carbon::createFromFormat('Y-m-d', $params['dublinCore']['@attributes']['created'])->toDateTimeString();

                     $params = $params['@attributes'];

                     unset($params['uRI']);
                     unset($params['typeURI']);

             
                    $result['parameters']['data'] = $params;



                }

                foreach($files['decorators'] as $decor_file)
                {

                    $decor = simplexml_load_file($decor_file);
                    $decor = json_decode(json_encode((array)$decor), TRUE);



             
                    $place =   $decor['@attributes']['targetISOKey'];
                    $place = explode("_", $place)[0];
                    $result['decorators'][] = [
                        'stat_key' => $place,
                        'stat_value' => $decor['@attributes']['infectiousCount'],
                        'type' => 'decorator',
                        'targetISOKey' => $decor['@attributes']['targetISOKey'],
                        'created' => Carbon::createFromFormat('Y-m-d', $decor['dublinCore']['@attributes']['created'])->toDateTimeString()
                       
                    ];
                }


                return $result;

            }


            return false;
    		
    	}

    	return false;

    }

    private function findAllFiles($dir) 
	{ 
		$result = [];
	    $root = array_slice(scandir($dir), 2);

	    $folder = $root[0];


	    $dir_list = File::glob("$dir/$folder/*", GLOB_ONLYDIR);


	    foreach ($dir_list as $list)
	    {
	    	
	    	$output = array();
			$chunks = explode('/', $list);



			$folder_name = $chunks[count($chunks) - 1];

			if ($folder_name == 'decorators')
			{

				$files = array_slice(scandir($list), 2);

				foreach ($files as $file)
				{	
		
					 if (!preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file)) {
					 	$result[] = "$dir/$folder/$folder_name/" .  $file;				
					 }
				}
			}

	    }

	    return $result;

	    
	}

    private function findScenario($dir)
    {
        $result = [];
        $folders = array_slice(scandir($dir), 2);



        foreach ($folders as $folder) 
        {
         
            $dir_list = File::glob("$dir/$folder/*", GLOB_ONLYDIR);

            foreach ($dir_list as $list)
            {
                $output = array();
                $chunks = explode('/', $list);

                $folder_name = $chunks[count($chunks) - 1];

              

                if ($folder_name == 'scenarios')
                {
                    

                    $files = array_slice(scandir($list), 2);


                    foreach ($files as $file)
                    {   
            
                         if (!preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file)) {
                            $scenario_file_path = "$dir/$folder/$folder_name/" .  $file;              
                         
                            $data = simplexml_load_file($scenario_file_path);

                            $array = json_decode(json_encode((array)$data), TRUE);

                            
                            //Get Decorators

                            if (array_key_exists('scenarioDecorators',$array))
                            {

                                foreach ($array['scenarioDecorators'] as $decorators)
                                {
                                    /*$format_decorators = explode('/', str_replace('#', '',$decorators['@attributes']['href']));

                                    $format_decorators = array_filter($format_decorators, 'strlen');

                                    $filename = end($format_decorators);
                                    $fname = prev($format_decorators);*/

                                    $scenario_paths =  $this->formatPath($decorators['@attributes']['href']);

                                    $result['decorators'][] =  "$dir/$folder/" . $scenario_paths;

                                }
                            }

                            if (array_key_exists('model',$array))
                            {
                                $model = $array['model']['@attributes']['href'];

                                $model =  $this->formatPath($model);

                                $model_file_path = "$dir/$folder/" .  $model;  

                                $data = simplexml_load_file($model_file_path);

                                $parameters = json_decode(json_encode((array)$data), TRUE);

                                $paramter_paths =  $this->formatPath($parameters['nodeDecorators']['@attributes']['href']);

                                $result['parameters'][] =  "$dir/$folder/" . $paramter_paths;

                            }

                         }



                    }

                }

            }

        
         

        }

        return $result;
    }


    private function formatPath($text)
    {
        $format_decorators = explode('/', str_replace('#', '',$text));

        $format_decorators = array_filter($format_decorators, 'strlen');

        $filename = end($format_decorators);
        $folder = prev($format_decorators);

        return $folder . '/' .  $filename;

    }

    private function moveFile($zip, $filename)
   	{

    	$zip->move($this->destination_path, $filename);

    	return $this;

   	}

    private function extract($filename,  $uncompressed_dir) 
    {
    	$zipPath = public_path($this->destination_path .  '/' . $filename);

    	$zipFile = Zip::open($zipPath);

    	$zipFile->extract($uncompressed_dir);

        exec("chmod -R 0777 $uncompressed_dir");

    	return $this;

    }
}
