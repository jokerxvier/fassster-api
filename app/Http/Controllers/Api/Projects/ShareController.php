<?php

namespace App\Http\Controllers\Api\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Projects;
use App\Models\Members;
use App\User;
use JWTAuth;

class ShareController extends ApiController
{

	protected $project;

    public function __construct(Projects $project)
    {
    	$this->project = $project;
    }

   
    public function project(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$rules = [
    		'project_id' => 'required|exists:projects,id',
        	'user_ids' => 'required|array', 
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	
    	$project_id = $request->has('project_id') ? $request->project_id : false;
    	$user_ids = $request->has('user_ids') ? $request->user_ids : false;

    	$user_collection = array_filter($user_ids, function($key) use ($user) {
		    return ($key != $user->id);
		});


    	$users = User::whereIn('id', $user_collection)->pluck('id')->toArray();
    	$project = $this->project::find($project_id);

    	if (!$project) { return $this->json_error(['success' => false,'message' => 'restricted_page']);  }

    	if (count($users) > 0)
    	{
    		$this->AddToProjectMembers($users, $project);
    	}

    	return $this->json(['data' => $project->members, 'success' => true]);
    }

    public function remove_project_members(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}


    	$rules = [
    		'project_id' => 'required|exists:projects,id',
        	'user_id' => 'required|exists:users,id', 
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	$members = Members::where('user_id', $request->user_id)->where('project_id', $request->project_id);

    	if ($members->count() <= 0)
    	{
    		return $this->json_error(['success' => false,'message' => 'user_not_found']); 
    	}

    	$members->delete();

    	$project = $this->project::find($request->project_id);

    	return $this->json(['data' => $project->members, 'success' => true]);


    }


    private function AddToProjectMembers($users, $project)
   	{
   		foreach($users as $user)
   		{
   			$project->members()->syncWithoutDetaching([$user => ['type' => 'member']]);
   		}

   	}


}
