<?php

namespace App\Http\Controllers\Api\Mdb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Diseases;
use App\Models\Sickness;
use DB;

class ReportController extends ApiController
{
	protected $limit = 10;
    public function __construct()
    {

    }

    public function index(Request $request)
    {

    	$rules = [
    		'yearFrom' => 'required',
    		'yearTo' => 'required',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	$arr = [];

    	$diseases = Diseases::select('disease_name')->groupBy('disease_name')
    				    ->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) BETWEEN  ? AND ?', [$request->input('yearFrom'), $request->input('yearTo')]);

        if ($request->has('location'))
        {

            $diseases = $diseases->where('Muncity', 'LIKE', "%" . strtoupper($request->input('location')) . "%")
                               ->orWhere('Province', 'LIKE', "%" . strtoupper($request->input('location')) . "%");  
        }

        if ($request->has('MorbidityWeek'))
        {
    

            $diseases = $diseases->whereRaw("CAST(MorbidityWeek AS UNSIGNED) = ?", [(int)$request->input('MorbidityWeek')]);            
        }
        
        $diseases = $diseases->get();


    	foreach($diseases as $disease)
    	{
    		$arg = Diseases::select(
    					DB::raw("COUNT(IF(Province !='',1,NULL)) AS provinceCount"),
    					DB::raw("COUNT(IF(Province ='',1,NULL)) AS provinceBlankCount"),

    					//province
    					DB::raw("COUNT(IF(Region !='',1,NULL)) AS regionCount"),
    					DB::raw("COUNT(IF(Region ='',1,NULL)) AS regionBlankCount"),

    					//province
    					DB::raw("COUNT(IF(Muncity !='',1,NULL)) AS cityCount"),
    					DB::raw("COUNT(IF(Muncity ='',1,NULL)) AS cityBlankCount"),

    					//age
    					DB::raw("COUNT(IF(AgeYears !='',1,NULL)) AS ageYearCount"),
    					DB::raw("MIN(CAST(AgeYears AS DECIMAL(4,2))) AS ageYearCountMin"),
    					DB::raw("MAX(CAST(AgeYears AS DECIMAL(4,2))) AS ageYearCountMax"),

    					//OutCome
    					DB::raw("COUNT(IF(Outcome ='A' OR Outcome ='ALIVE',1,NULL)) AS aliveCount"),
    					DB::raw("COUNT(IF(Outcome ='D' OR Outcome ='DEAD',1,NULL)) AS deadCount"),
    					DB::raw("COUNT(IF(Outcome ='',1,NULL)) AS blankCount"),
    					DB::raw("COUNT(Outcome) AS OutcomeTotalCount"),

    					//MobidityWeek
    					DB::raw("COUNT(MorbidityWeek) AS mobidityCount"),
    					DB::raw("MIN(CAST(MorbidityWeek AS UNSIGNED)) AS mobidityCountMin"),
    					DB::raw("MAX(CAST(MorbidityWeek AS UNSIGNED)) AS mobidityCountMax"),
    					DB::raw("COUNT(IF(MorbidityWeek ='',1,NULL)) AS mobidityBlankCount"),

    					DB::raw("COUNT(Dadmit) AS dAdmitCount"),
    					DB::raw('MIN(STR_TO_DATE(Dadmit, "%m/%d/%Y")) AS dAdmitMin'),
    					DB::raw('MAX(STR_TO_DATE(Dadmit, "%m/%d/%Y")) AS dAdmitMax')

    				)
    				->where('disease_name', $disease->disease_name)
    				->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) BETWEEN  ? AND ?', [$request->input('yearFrom'), $request->input('yearTo')]);

                    if ($request->has('location'))
                    {

                        $arg = $arg->where('Muncity', 'LIKE', "%" . strtoupper($request->input('location')) . "%")
                                           ->orWhere('Province', 'LIKE', "%" . strtoupper($request->input('location')) . "%");  
                    }

                    if ($request->has('MorbidityWeek'))
                    {

                   
                        $arg = $arg->whereRaw("CAST(MorbidityWeek AS UNSIGNED) = ?", [(int)$request->input('MorbidityWeek')]);      
                    }
    	
    				$arg = $arg->groupBy('disease_name')->first();


    		$arr[$disease->disease_name] = [

    			'Region' => [
    				'count' =>  $arg->regionCount,
    				'blank' => $arg->regionBlankCount,
    			],

    			'Province' => [
    				'count' =>  $arg->provinceCount,
    				'blank' => $arg->provinceBlankCount,
    			],

    			'Muncity' => [
    				'count' =>  $arg->cityCount,
    				'blank' => $arg->cityBlankCount,
    			],

    			'Age' => [
    				'count' => $arg->ageYearCount,
    				'min' => $arg->ageYearCountMin,
    				'max' => $arg->ageYearCountMax,
    			],

    			'OutCome' => [
    				'count' => $arg->OutcomeTotalCount,
    				'A' => $arg->aliveCount,
    				'D' => $arg->deadCount,
    				'blank' => $arg->blankCount
    			],

    			'MobidityWeek' => [
    				'count' => $arg->OutcomeTotalCount,
    				'min' => $arg->mobidityCountMin,
    				'max' => $arg->mobidityCountMax,
    				'blank' => $arg->mobidityBlankCount
    				
    			],

    			'Dadmit' => [
    				'count' => $arg->dAdmitCount,
    				'min' => $arg->dAdmitMin,
    				'max' => $arg->dAdmitMax,
    				
    				
    			],


    		];
    	}

    	return $this->json(['data' => $arr, 'success' => true]);


    }
}
