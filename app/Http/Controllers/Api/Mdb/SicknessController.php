<?php

namespace App\Http\Controllers\Api\Mdb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Sickness;

class SicknessController extends ApiController
{

    public function __construct()
    {
    	
    }

    public function index()
    {

    	$sickness = Sickness::all();

    	return $this->json(['data' => $sickness, 'success' => true]);
    }
}
