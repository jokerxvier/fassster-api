<?php

namespace App\Http\Controllers\Api\Mdb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Diseases;
use DB;

class FilterController extends ApiController
{
	protected $limit = 50;
	
    public function __construct()
    {

    }

    public function index(Request $request)
    {
  
    
    	if ($request->has('yearFrom')) {
	    	$fromData = Diseases::select('disease_name', 
	    		DB::raw("COUNT(IF(Outcome='Alive' OR Outcome='A',1,NULL)) AS alive"), 
	    		DB::raw("COUNT(IF(Outcome='Dead' OR Outcome='D',1,NULL)) AS dead")

	    	)->filter($request)->groupBy('disease_name')
	    	->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) = ?', $request->input('yearFrom'))->get();

	    	$arr[$request->input('yearFrom')] = $fromData;
	    }

	    if ($request->has('yearTo')) {
	    	$fromData = Diseases::select('disease_name', 
	    		DB::raw("COUNT(IF(Outcome='Alive' OR Outcome='A',1,NULL)) AS alive"), 
	    		DB::raw("COUNT(IF(Outcome='Dead' OR Outcome='D' ,1,NULL)) AS dead")

	    	)->filter($request)->groupBy('disease_name')
	    	->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) = ?', $request->input('yearTo'))->get();

	    	$arr[$request->input('yearTo')] = $fromData;
	    }


    	
	    return $this->json(['data' => $arr, 'success' => true]);


    }

    public function reportV2(Request $request)
    {
  		$arr = [];
  		$arrFrom = [];
  		$arrTo = [];
    
    	if ($request->has('yearFrom')) {
    		

	    	$fromData = Diseases::select('disease_name', 
	    		DB::raw("COUNT(IF(Outcome='Alive' OR Outcome='A',1,NULL)) AS alive"), 
	    		DB::raw("COUNT(IF(Outcome='Dead' OR Outcome='D',1,NULL)) AS dead")

	    	)->filter($request)->groupBy('disease_name')
	    	->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) = ?', $request->input('yearFrom'))->get();

	    	$arrFrom['data']['year'] = $request->input('yearFrom');
	    	$arrFrom['data']['details'] = $fromData;


	    	//$arr[$request->input('yearFrom')] = $fromData;
	    }

	    if ($request->has('yearTo')) {
	    	$fromData = Diseases::select('disease_name', 
	    		DB::raw("COUNT(IF(Outcome='Alive' OR Outcome='A',1,NULL)) AS alive"), 
	    		DB::raw("COUNT(IF(Outcome='Dead' OR Outcome='D' ,1,NULL)) AS dead")

	    	)->filter($request)->groupBy('disease_name')
	    	->whereRaw('YEAR(STR_TO_DATE(Dadmit, "%m/%d/%Y")) = ?', $request->input('yearTo'))->get();

	    	$arrTo['data']['year'] = $request->input('yearTo');
	    	$arrTo['data']['details'] = $fromData;

	    	//$arr[$request->input('yearTo')] = $fromData;
	    }


	    $arr = [$arrFrom, $arrTo];


    	
	    return $this->json(['data' => $arr, 'success' => true]);


    }


}
