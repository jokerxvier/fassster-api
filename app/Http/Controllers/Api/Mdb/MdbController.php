<?php

namespace App\Http\Controllers\Api\Mdb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Diseases;
use App\Models\MdbFiles;
use Config, JWTAuth, Excel, File, DB;
ini_set('max_execution_time', 180);
class MdbController extends ApiController
{

	//install mdbtools
	protected $destination_path;
    protected $limit = 10;

    public function __construct()
    {
    	$this->destination_path = Config::get('fassster.upload.mdb');
    }

    public function index()
    {

        $arr = [];
        $arg = [];

    	$diseases = Diseases::select('Region', 'Province', 'Muncity', 'disease_name')->groupBy('disease_name', 'Province', 'Region', 'Muncity')->get();

    	foreach($diseases as $d)
    	{


           $data  = Diseases::where('disease_name', $d->disease_name)
                            ->where('Region', $d->Region)
                            ->where('Muncity', $d->Muncity)
                            ->get();

            
            $arr[$d->disease_name][] = [
                 "Disease" => $d->disease_name,
                 "Region" => $d->Region,
                 "Province" => $d->Province,
                 "City" => $d->Muncity,
                 'data' => $data
            ];
     

       }
        
       return $this->json(['data' => $arr, 'success' => true]);



    }

    public function upload(Request $request)
    {
  		
    	
    	$rules = [
    		'mdb' => 'required|mimes:mdb',
    	];


    	$validate = $this->validateRequest($request->all(), $rules);
    	
    	if ($validate) { return $validate; }

    	if ($request->hasFile('mdb')) 
    	{

    		$file = $request->file('mdb');

    		$disease_name = $this->removeFileExtension($file);

    		$new_file_name =  time();

    		$filename = $new_file_name  .'.'.$file->getClientOriginalExtension();

    		$this->moveFile($file, $filename);


    		//Get the tablename
    		$tableName = shell_exec("cd $this->destination_path && mdb-tables $filename"); 

    		$tableName = trim(preg_replace('/\s\s+/', ' ', $tableName));


    		$uploaded_csv = $this->destination_path . '/' . $filename . '.csv';

    		shell_exec("cd $this->destination_path && mdb-export $filename $tableName  > $filename.csv");    
    	
    		if (file_exists($uploaded_csv))
			{ 
				$inserted_data = $this->importExcel($uploaded_csv, $tableName);

				File::delete($this->destination_path . '/' . $filename ); //delete MDB after use
				File::delete($uploaded_csv); //delete CSV after use

				return $this->json(['message' => 'upload_success', 'success' => true]);



			}else {
				return $this->json_error(['message' => 'uploaded_file_not_found', 'success' => false]);
			}



    	}

    	return $this->json_error(['message' => 'invalid_file', 'success' => false]);

    }

    public function upload_version_two(Request $request)
    {
        
        
        $rules = [
            'mdb' => 'required|mimes:mdb',
            'sickness_id' => 'required|exists:sickness,id',
        ];

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }



        $validate = $this->validateRequest($request->all(), $rules);
        
        if ($validate) { return $validate; }

        if ($request->hasFile('mdb')) 
        {

            $file = $request->file('mdb');

            $original_filename = $file->getClientOriginalName();

            $disease_name = $this->removeFileExtension($file);

            $new_file_name =  time();

            $filename = $new_file_name  .'.'.$file->getClientOriginalExtension();

            $this->moveFile($file, $filename);


            //Get the tablename
            $tableName = shell_exec("cd $this->destination_path && mdb-tables $filename"); 

            $tableName = trim(preg_replace('/\s\s+/', ' ', $tableName));


            $uploaded_csv = $this->destination_path . '/' . $filename . '.csv';

            shell_exec("cd $this->destination_path && mdb-export $filename $tableName  > $filename.csv");    
        
            if (file_exists($uploaded_csv))
            { 
                //Insert MDB FILES

                $mdb_files = MdbFiles::create(['name' => $original_filename, 'user_id' => $user->id, 'sickness_id' => $request->sickness_id]);

                $inserted_data = $this->importExcel($uploaded_csv, $mdb_files);

                File::delete($this->destination_path . '/' . $filename ); //delete MDB after use
                File::delete($uploaded_csv); //delete CSV after use

                return $this->json(['message' => 'upload_success', 'success' => true]);



            }else {
                return $this->json_error(['message' => 'uploaded_file_not_found', 'success' => false]);
            }



        }

        return $this->json_error(['message' => 'invalid_file', 'success' => false]);

    }

    public function delete_mdb_files(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $rules = [
            'mdb_file_id' => 'required|exists:mdb_files,id'
        ];

        $validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
        if ($validate) { return $validate; }

        $mdb = MdbFiles::find($request->mdb_file_id);
        $mdb->delete();

        return $this->json(['message' => 'delete_success', 'success' => true]);


    }

    public function uploaded_list()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $mdb = MdbFiles::where('user_id',  $user->id)->paginate($this->limit);

        return $this->json(['data' => $mdb, 'success' => true]);

    }



    private function importExcel($path, $mdb_files)
    {

        Excel::filter('chunk')->load($path)->chunk(250, function($results) use($mdb_files) {
                $inserted_data = [];

                foreach($results as $result)
                {
                    $muncity = strtoupper(rtrim($result->muncity));
                    $region = strtoupper(rtrim($result->region));
                    $province = strtoupper(rtrim($result->province));
                    $barangay = strtoupper(rtrim($result->barangay));
                    $dAdmit = rtrim($result->dadmit); //date
                    $dOnset = rtrim($result->donset); //date
                    $outcome = strtoupper(rtrim($result->outcome));
                    $morbidWeek = rtrim($result->morbidityweek);
                    $classification = (isset($result->caseclassification)) ?  strtoupper(rtrim($result->caseclassification)) : "";
                    $age = empty($result->ageyears) ? 0 :  round((float) $result->ageyears, 2);
                    $sex =  strtoupper($result->sex);

                    $is_exist = Diseases::where('disease_name', $mdb_files->sickness->name)
                                ->where('Region', $region)
                                ->where('Province', $province)
                                ->where('Muncity', $muncity) 
                                ->where('Barangay', $barangay) 
                                ->where('DAdmit', $dAdmit) 
                                ->where('DOnset', $dOnset) 
                                ->where('Outcome', $outcome) 
                                ->where('AgeYears', $age)
                                ->where('Sex', $sex)
                                ->where('MorbidityWeek', $morbidWeek) 
                                ->where('Classification', $classification)
                                ->count();

                    if ($is_exist <= 0)
                    {
                        $inserted_data[] = [
                            'disease_name' => $mdb_files->sickness->name,
                            'mdb_file_id' => $mdb_files->id,
                            'Region' => $region,
                            'Province' => $province,
                            'Barangay' => $barangay,
                            'DAdmit' => $dAdmit,
                            'DOnset' => $dOnset,
                            'Outcome' => $outcome,
                            'AgeYears' => $age,
                            'Sex' => $sex,
                            'MorbidityWeek' => $morbidWeek,
                            'Muncity' => $muncity,
                            'Classification' => $classification

                        ];
                    }
                }

                //Diseases::insert($inserted_data);

                if (count($inserted_data) > 0) {
                    DB::table('diseases')->insert($inserted_data);
                }else {
                    return $this;
                }

        });

        return $this;

    }

    private function removeFileExtension($file)
    {

    	$fileName = basename($file->getClientOriginalName());
		$fileNameNoExtension = preg_replace("/\.[^.]+$/", "", $fileName);

		return $fileNameNoExtension;

    }

     private function moveFile($file, $filename)
   	{

    	$file->move($this->destination_path, $filename);

    	return $this;

   	}
}
