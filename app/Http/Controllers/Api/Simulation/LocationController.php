<?php

namespace App\Http\Controllers\Api\Simulation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Simulations;

class LocationController extends ApiController
{

    public function region()
    {
    	$regions = Simulations::groupBy('region')->pluck('region')->toArray();
    	return $this->json(['data' => $regions, 'success' => true]);
    }

    public function province(Request $request)
    {
    	$region = $request->has('region') ? $request->region : false;

    	$province = Simulations::where('region', $region)->groupBy('province')->pluck('province')->toArray();

    	return $this->json(['data' => $province, 'success' => true]);

    }

    public function city(Request $request)
    {
    	$province = $request->has('province') ? $request->province : false;

    	$city = Simulations::where('province', $province)->groupBy('city')->pluck('city')->toArray();

    	return $this->json(['data' => $city, 'success' => true]);

    }
}
