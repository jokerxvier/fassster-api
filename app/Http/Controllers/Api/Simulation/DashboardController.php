<?php

namespace App\Http\Controllers\Api\Simulation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Simulations;
use App\Models\Scenarios;
class DashboardController extends ApiController
{
   public function __construct()
   {

   }

   public function graph(Request $request)
   {
   	  $rules = [
    		'scenario_id' => 'required|exists:scenarios,id',
    	];

    	$validate = $this->validateRequest($request->all(), $rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	$scenario = Scenarios::find($request->scenario_id);

    	$simulations = Simulations::select('name')
                        ->where('type', 'I')
                        ->filter($request)
                        ->groupBy('name');
      $arr = [];
      $arg = [];

      if ($simulations->count() > 0)
      {


        foreach($simulations->pluck('name') as $location)
        {

            $arr['location'] = $location;
            $arr['count'] =  Simulations::where('type', 'I')->where('name', $location)->filter($request)->pluck('value')->toArray();

            $arg[] = $arr;

          //$arg[$simulation->name] = $simulation->name;
          //$arg[$simulation->name]['count'][] = $simulation->value;
         
        }



      	return $this->json(['data' => $arg,  'success' => true]);
      }

      return $this->json(['data' => [],  'success' => true]);
   }


}
