<?php

namespace App\Http\Controllers\Api\Simulation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Simulations;
use App\Models\Scenarios;
use JWTAuth, DB;

class SimulationController extends ApiController
{
    protected $limit = 10;
    public function __construct()
    {

    }

    public function index(Request $request)
    {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $rules = [
            'scenario_id' => 'required|exists:scenarios,id',
        ];


        $data = Simulations::where('scenario_id', $request->scenario_id)->paginate($this->limit);


        

        $validate = $this->validateRequest($request->all(), $rules);

        //validate data
        if ($validate) { return $validate; }

        return $this->json(['data' => $data,  'success' => true]);

    }

    public function insert()
    {
        $json_file = public_path('simulation.json');

        $json = file_get_contents($json_file);

        $result = json_decode($json, true);

        
        Simulations::insertSimulation($result['data']);
        
    }

   public function map(Request $request)
   {
        $scenario = Scenarios::find($request->scenario_id);

        if (!$scenario) { 
            return $this->json_error(['success' => false,'message' => 'scenario_not_found']);  
        }

        $data = [
            'date_length' => 0,
            'grades' => [],
            'data' => []
        ];

        $grades = [];

        $incremental = 0;

        // $simulations = Simulations::select(DB::raw('name as location'), DB::raw('GROUP_CONCAT( CAST(value AS UNSIGNED) ORDER BY value ASC ) as count'))->filter($request)->groupBy('name');

        $simulations = Simulations::select(DB::raw('name as location'), DB::raw('CAST(value AS UNSIGNED) as count'))->filter($request)->get();

       


        if ($simulations->count() > 0)
        {

            $total_days = Simulations::selectRaw("MAX(CAST(iteration AS UNSIGNED)) as num")->filter($request)->first()->num;
            $max = Simulations::selectRaw("MAX(CAST(value AS UNSIGNED)) as num")->filter($request)->first()->num;
            $min = Simulations::selectRaw("MIN(CAST(value AS UNSIGNED)) as num")->filter($request)->first()->num;
            $dates = Simulations::select("iteration", "simulation_date")->filter($request)->orderBy(DB::raw('cast(iteration as unsigned)'), 'ASC')
                                ->groupBy('iteration', 'simulation_date')
                                ->get();

            $d = [];

            foreach($dates as $date) {
                array_push($d, $date->simulation_date);
            }

            $data['dates'] = $d;

            $data['date_length'] = $total_days;


           
            $max = (int) $this->roundDown($max);
            $min = (int) $this->roundDown($min);

            if ($max > $min)
            {
                $incremental = (int) (($max - $min) / 8);
            }else{
                $incremental = (int) (($min - $max) / 8);
            }

            foreach (range(1, 8) as $i) {
               $grades[] = $i * $incremental;
            }

            $data['grades'] = $grades;

            $collection = $simulations;



            
            $result = [];

            foreach($simulations as $simulation)
            {
                $result[$simulation->location]['location'] = $simulation->location;
                $result[$simulation->location]['count'][] = $simulation->count;
                
            }

            $data['data'] = $result;

            return $this->json(['data' => $data,  'success' => true]);

        }
    

       return $data;

   }

   private function roundDown($value)
   {
        $parts = explode('.', (string) $value);

        return (int) $parts[0];
   }


}
