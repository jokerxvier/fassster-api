<?php

namespace App\Http\Controllers\Api\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;

class RoleController extends ApiController
{

	public function __construct()
	{

	}

	public function index()
	{
		$roles = Role::all(['id', 'name']);

		return $this->json(['data' => $roles, 'success' => true]);
	}
    
}
