<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserInformation;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator, Carbon\Carbon, JWTAuth;

class RegisterController extends ApiController
{
	
	protected $users;
	protected $role;

    public function __construct(User $users, Role $role)
    {
    	$this->users = $users;
    	$this->role = $role;

    }

    public function register(Request $request)
   	{

      $validate = $this->validateRequest($request->all(), $this->users::$rules);
        
        //validate data
    	if ($validate) { return $validate; }

    	$user_params = $request->only(['fname', 'lname', 'email', 'password']);
    	$user = $this->users::create($user_params);

    	//save user information and assign to a role
    	$this->saveUserInformation($user, $request)
    		->assignToRole($user, $request);

    	return $this->json(['success' => true, 'data' => $user]);
   	}


   	private function saveUserInformation($user, $request)
   	{
   		$info = new UserInformation;
   		$info->region = $request->has('region') ? $request->region : '';
   		$info->province = $request->has('province') ? $request->province : '';
   		$info->municipality = $request->has('municipality') ? $request->municipality : '';
   		$info->address = $request->has('address') ? $request->address : '';
      $info->company = $request->has('company') ? $request->company : '';

   		$user->information()->save($info);

   		return $this;

   	}

   	private function assignToRole($user, $request)
   	{
   		$role = $this->role::find($request->roles);

   		if ($role) { $user->assignRole($role->name); }

   		return $this;
   	}


}
