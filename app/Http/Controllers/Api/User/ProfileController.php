<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\User;
use Bican\Roles\Models\Role;
use App\Models\UserInformation;
use JWTAuth;

class ProfileController extends ApiController
{
	protected $users;

	protected  $rules = [
        'fname' => 'sometimes|required|max:255',
        'lname' => 'sometimes|required|max:255',
        'password' => 'sometimes|required||min:4|max:255',
        'roles' => 'sometimes|exists:roles,id',
        'region' => 'sometimes|max:255', 
        'municipality' => 'sometimes|max:255', 
        'address' => 'sometimes|max:255', 
        'province' => 'sometimes|max:255', 
        'company' => 'sometimes|max:255', 
    ];

    public function __construct(User $users)
    {
    	$this->users = $users;
    }


    public function index(Request $request)
    {
        $user_data = [];

    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

        if ($request->has('user_id'))
        {
            $user_data = $this->users->find($request->user_id);
        }else {
            $user_data = $this->users->find($user->id);
        }


		

		if (!$user_data) { return $this->json_error(['success' => false,'message' => 'user_not_found']); }

		return $this->json(['data' => $user_data, 'success' => true]);

    }

    public function update(Request $request)
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
			return $this->json_error(['success' => false,'message' => 'user_not_found']);
		}

		$validate = $this->validateRequest($request->all(), $this->rules);

    	if ($validate) {
    		return $validate;
    	}

		$user_data = $this->users->find($user->id);
		

		$this->updateData($user_data, $request)
			->updateAddedInformation($user_data, $request);

	

		return $this->json(['data' => $user_data, 'success' => true]);

    }

    public function activate_user(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }


        $user_id = $request->has('user_id') ? $request->user_id : 0;

        $activate_user = $this->users->find($user_id);

        if (!$activate_user)
        {
            return $this->json_error(['success' => false,'message' => 'not_exist']);
        }

        if (!$activate_user->is_active)
        {

            if ($user->hasRole($this->users::$adminRole) || $user->hasRole($this->users::$superAdminRole))
            {
                $activate_user->is_active = true;
                $activate_user->save();

                return $this->json(['success' => true,'message' => 'activate_success']);

                
            }else {
               return $this->json_error(['success' => false,'message' => 'unauthorized']);
            }

        }else {
            
            return $this->json_error(['success' => false,'message' => 'user_already_activated']);
        }

       
    }
    


    private function updateData($user_data, $request)
    {

    	if ($request->has('fname') && $user_data->fname != $request->fname)
		{
			$user_data->fname = $request->fname;
		}

		if ($request->has('lname') && $user_data->lname != $request->lname)
		{
			$user_data->lname = $request->lname;
		}

		if ($request->has('password'))
		{
			$user_data->password = $request->password;
		}

		$user_data->save();

		return $this;

    }

    private function updateAddedInformation($user_data, $request)
    {
    	$arr = [];


    	$info = $request->only('region', 'municipality', 'address', 'province', 'company');
    	

		$user_data->information()->update($info);

		return $this;

    }


}
