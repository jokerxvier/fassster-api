<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\User;
use JWTAuth;


class LoginController extends ApiController
{
	protected $auth; 
	protected $user;

	protected $rules = [
		'email' => 'required|email',
		'password' => 'required',
	];

    public function __construct(JWTAuth $auth, User $user)
    {
    	$this->auth = $auth;
    	$this->user = $user;
    }

    public function login(Request $request)
    {


    	$validate = $this->validateRequest($request->all(), $this->rules);

    	if ($validate) {
    		return $validate;
    	}

        if (User::where('email', $request->email)->where('is_active', false)->count() > 0)
        {
            return $this->json_error(['success' => false,'message' => 'not_active']);
        }


    	$token = $this->authenticate($request);

    	if (!$token)
    	{        	
            return $this->json_error(['success' => false,'message' => 'failed_to_login']);
    	}
    	
    	
        $user = $this->user->where('email', $request->email)->first();

        return $this->json(['data' => $user, 'token' => $token, 'success' => true]);

    }

    public function refresh_token()
    {
        

        $token = $this->auth::getToken();


        if(!$token){
            return $this->json_error(['success' => false,'message' => 'token_not_provided']);
        }

        try{
            $token = $this->auth::refresh($token);
        }catch(TokenInvalidException $e){
           
            return $this->json_error(['success' => false,'message' => 'invalid_token']);
        }


        return $this->json(['token' => $token, 'success' => true]);
    }


    private function authenticate($request)
    {

    	$credentials = $request->only('email', 'password');

    	try {
            if (! $token =  $this->auth::attempt($credentials)) {
                return false;
            }
        } catch (JWTException $e) {
            return false;
        }

        return $token;
    }


}
