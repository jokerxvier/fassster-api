<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\User;
use Bican\Roles\Models\Role;
use App\Models\UserInformation;
use JWTAuth;

class UserController extends ApiController
{
    protected $users;
    protected $limit = 10;

    public function __construct(User $users)
    {
    	$this->users = $users;
    }

    public function active()
    {
        $user = [];

    	if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }   

        $region = $user->information->region;

        if ($user->hasRole($this->users::$superAdminRole))
        {

             $users = $this->users->where('is_active', true)->paginate(10);

             return $this->json(['data' => $users, 'success' => true]);
        }

        if ($user->hasRole($this->users::$adminRole))
        {
           
                $users = $this->users->where('is_active', true)
                    ->exceptAdmin()
                    ->whereHas('information', function ($q) use($region) {
                        $q->where('region',  $region);
                    })->paginate(10);
        

           
            return $this->json(['data' => $users, 'success' => true]);

        }else {
            return $this->json_error(['success' => false,'message' => 'unauthorized']);
        }

        

    }

    public function pending()
    {
    	if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }

        $region = $user->information->region;



        if ($user->hasRole($this->users::$superAdminRole))
        {


             $users = $this->users->where('is_active', false)->paginate(10);

             return $this->json(['data' => $users, 'success' => true]);
        }

        
        if ($user->hasRole($this->users::$adminRole))
        {

            $users = $this->users->where('is_active', false)
                ->exceptAdmin()
                ->whereHas('information', function ($q) use($region) {
                $q->where('region',  $region);
            })->paginate(10);

            return $this->json(['data' => $users, 'success' => true]);

        }else {
            return $this->json_error(['success' => false,'message' => 'unauthorized']);
        }

    }

    public function deactivate(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return $this->json_error(['success' => false,'message' => 'user_not_found']);
        }


        $user_id = $request->has('user_id') ? $request->user_id : 0;

        $activate_user = $this->users->find($user_id);

        if (!$activate_user)
        {
            return $this->json_error(['success' => false,'message' => 'not_exist']);
        }

      
        if ($user->hasRole($this->users::$adminRole) || $user->hasRole($this->users::$superAdminRole))
        {
            $activate_user->is_active = false;
            $activate_user->save();

            return $this->json(['success' => true,'message' => 'deactivate_success']);

            
        }else {
           return $this->json_error(['success' => false,'message' => 'unauthorized']);
        }
       
    }
}
