<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Simulations;
use App\Models\Scenarios;
class StemParseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stem:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $scenarios = Scenarios::where('status', 'queued')
          ->take(100)
          ->chunk(100, function ($scenarios) {
            foreach($scenarios as $scenario) {
              $url = env('STEM_URL', false) .'/getJobStatus?job_id=' . $scenario->order_id;
              $json = file_get_contents($url);
              $res = json_decode($json, true);

              if (array_key_exists('success', $res) && array_key_exists('data', $res)) {

                if ($res['success'] == 'success' && count($res['data']) > 0) {
                  $scenario->simulations()->delete();
                  Simulations::insertSimulation($res['data'], $scenario->id);

                  $scenario->status = 'active';
                  $scenario->save();
                }
              }

              if (array_key_exists('status', $res)) {

                if ($res['status'] == 'stem_error' && $res['success'] == 'error') {
                  $scenario->status = 'error';
                  $scenario->save();
                }
                 
              }
            }
        });
        
        
        /*Scenarios::where('status', 'queued')
          ->chunk(1, function ($scenarios){
            foreach($scenarios as $scenario)
            {
              $url = env('STEM_URL', false) .'/getJobStatus?job_id=' . $scenario->order_id; //replace this when kyles api is fixed
              $json = file_get_contents($url);
              $res = json_decode($json, true);

 
              if (array_key_exists('success', $res) && array_key_exists('data', $res)) {

                if ($res['success'] == 'success' && count($res['data']) > 0) {
                  $scenario->simulations()->delete();
                  Simulations::insertSimulation($res['data'], $scenario->id);

                  $scenario->status = 'active';
                  $scenario->save();
                }
              }

              if (array_key_exists('status', $res)) {

                if ($res['status'] == 'stem_error' && $res['success'] == 'error') {
                  $scenario->status = 'error';
                  $scenario->save();
                }
                 
              }
            }
          });*/
    }
}
