<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Scenarios;

class UploadStem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stem:upload';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target_url = env('STEM_URL', false) . '/upload';

         Scenarios::where('status', 'pending')
          ->chunk(100, function ($scenarios) use($target_url){

            foreach($scenarios as $scenario)
            {
              $file_path = public_path($scenario->zip_path);

              //$cfile = new \CurlFile($file_path,'application/zip', $scenario->title);

              $cfile = $this->getCurlValue($file_path,'application/zip','cattle-01.zip');
              

              $post = array('file'=> $cfile, 'disease_name' => $scenario->project->disease_name);
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL,$target_url);
              curl_setopt($ch, CURLOPT_POST,1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
              $result=curl_exec ($ch);
              $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
              curl_close ($ch);
              if ($httpCode == 200) {
                $res =  json_decode($result, true);
                if (array_key_exists('success', $res)) {
                  if ($res['success'] == 'success') {
                    $scenario->status = 'queued';
                    $scenario->order_id = $res['job_id'];
                    $scenario->save();
                  }
                }
              }
            }
          });
        
    }

    private function getCurlValue($filename, $contentType, $postname)
    {
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }
        $value = "@{$filename};filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }
     
        return $value;
    }
}
