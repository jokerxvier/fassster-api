<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Simulations extends Model
{
	use SoftDeletes;
    protected $table = 'simulations';
    protected $primaryKey = 'id';
    protected $fillable = ['json_result', 'name' ,'scenario_id', 'iteration', 'simulation_date', 'region', 'province', 'city', 'type', 'value','created_at', 'updated_at'];

    public function getProvinceAttribute($value)
    {
    	return ucfirst($value);
    }

    public function getCityAttribute($value)
    {
    	return ucfirst($value);
    }

    public function getValueAttribute($value)
    {
    	$parts = explode('.', (string) $value);
    	return (int) $parts[0];
    }

    public static function insertSimulation($result, $scenario_id = null)
    {
     
        if (key_exists('S_0', $result)) {
            self::formatSimulation($result['S_0'], 'S', $scenario_id);
        }

        if (key_exists('E_0', $result )) {
            self::formatSimulation($result['E_0'], 'E', $scenario_id);
        }

        if (key_exists('I_0', $result)) {
            self::formatSimulation($result['I_0'], 'I', $scenario_id);
        }

        if (key_exists('R_0', $result)) {
            self::formatSimulation($result['R_0'], 'R', $scenario_id);
        }
    }

    private static function formatSimulation($data, $type, $scenario_id) {

        $arr = [];
        $arg = [];

        $arr['type'] =  $type;
        $arr['scenario_id'] = $scenario_id;
        $arr['created_at'] = Carbon::now();
        $arr['updated_at'] = Carbon::now();

        foreach($data as $d) {
            if (array_key_exists('iteration', $d))
            {
                $iteration =  $d['iteration'];

                $arr['iteration'] = $iteration;
                unset($d['iteration']);
            }

            if (array_key_exists('time', $d))
            {   
                $time = $d['time'];

                $arr['simulation_date'] = $time;
                unset($d['time']);
            }

            foreach($d as $key => $value) {
                $arr['value'] = $value;
                $arr['name'] = $key;
                $location = explode('_', $key);

                $city = $location[0];
                $province = $location[1];
                $region = $location[2];


                $arr['city'] = $city;
                $arr['province'] = $province;
                $arr['region'] = $region;
                $arg[] = $arr;
            }
        }

        if (count($arg) > 0) {
            foreach (array_chunk($arg, 1000) as $res) {
                Simulations::insert($res);
            }
        }
        
        //DB::table('simulations')->insert($arg);
        //Simulations::insert($arg);
        return;
    }

    public function scopeFilter($query, $request)
    {

        if ($request->has('region')) {
        	$query->where('region', $request->input('region'));
    	}

    	if ($request->has('province')) {
        	$query->where('province', strtolower($request->input('province')));
    	}

    	if ($request->has('city')) {
        	$query->where('city', strtolower($request->input('city')));
    	}

        if ($request->has('type')) {
            $query->where('type', strtolower($request->input('type')));
        }

        if ($request->has('scenario_id')) {
            $query->where('scenario_id', $request->input('scenario_id'));
        }

    	
    	return $query;
        
    }

}
