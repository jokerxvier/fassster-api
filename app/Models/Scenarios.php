<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Statistics;
use Auth, File, Zip;




class Scenarios extends Model
{
    use SoftDeletes;

    protected $table = 'scenarios';
    protected $primaryKey = 'id';
    protected $appends = ['owner', 'members', 'ownership_type', 'parameters_data', 'decorators_data'];
    protected $hidden = ['deleted_at', 'zip_path'];
    protected $fillable = ['title', 'description', 'project_id', 'type', 'zip_path', 'created', 'order_id'];
    protected $dates = ['deleted_at'];


    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }


    public function project()
    {
        return $this->belongsTo('App\Models\Projects', 'project_id');
    }

    public function user_member()
    {
        return $this->hasMany('App\Models\Members', 'scenario_id');
    }

    public function statistics()
    {
        return $this->hasMany('App\Models\Statistics', 'scenario_id');
    }

    public function simulations()
    {
        return $this->hasMany('App\Models\Simulations', 'scenario_id');
    }

    public function decorators()
    {
        return $this->hasMany('App\Models\Statistics', 'scenario_id')->where('type', 'decorator');
    }

    public function parameters()
    {
        return $this->hasMany('App\Models\Statistics', 'scenario_id')->where('type', 'parameter');
    }

    public function owner()
    {
        return $this->belongsToMany('App\User', 'members', 'scenario_id', 'user_id')
                    ->where('type', 'owner')
                    ->withTimestamps();
    }

    public function members()
    {
        return $this->belongsToMany('App\User', 'members', 'scenario_id', 'user_id')
                    ->where('type', 'member')
                    ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'members', 'scenario_id', 'user_id');
    }

    public function getOwnerAttribute()
    {
        return $this->owner()->count() > 0 ?   $this->owner()->first()->name : '';
    }

    public function getOwnershipTypeAttribute()
    {
        $current = Auth::user()->id;
        $membership = $this->user_member()->where('user_id', $current);
        return $membership->count() > 0 ? $membership->first()->type  : '';
    }

    public function getMembersAttribute()
    {
        return $this->members()
                ->get(['fname', 'lname', 'users.id', 'type'])
                ->makeHidden(['additional_information'])->toArray();
    }

    public function getParametersDataAttribute()
    {
        return $this->parameters()->get();
    }

    public function getDecoratorsDataAttribute()
    {
        return $this->decorators()->get();
    }

    public function scopeSearch($query, $search)
    {
        // Split each Name by Spaces
        $searchs = $search;

        // Search each Name Field for any specified Name
        return Scenarios::where(function($query) use ($searchs) {
            $query->where('title', 'like' , "%{$searchs}%");
            $query->orWhere(function($query) use ($searchs) {
                 $query->where('description', 'like' , "%{$searchs}%");
            });
        });
    }

    public function findAllFiles($dir) 
    { 
        $result = [];
        $folders = array_slice(scandir($dir), 2);



        foreach ($folders as $folder) 
        {
         
            $dir_list = File::glob("$dir/$folder/*", GLOB_ONLYDIR);

            foreach ($dir_list as $list)
            {
                $output = array();
                $chunks = explode('/', $list);

                $folder_name = $chunks[count($chunks) - 1];

              

                if ($folder_name == 'scenarios')
                {
                    

                    $files = array_slice(scandir($list), 2);


                    foreach ($files as $file)
                    {   
            
                         if (!preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file)) {
                            $scenario_file_path = "$dir/$folder/$folder_name/" .  $file;              
                         
                            $data = simplexml_load_file($scenario_file_path);

                            $array = json_decode(json_encode((array)$data), TRUE);

                            
                            //Get Decorators

                            if (array_key_exists('scenarioDecorators',$array))
                            {

                                foreach ($array['scenarioDecorators'] as $decorators)
                                {


                                    /*$format_decorators = explode('/', str_replace('#', '',$decorators['@attributes']['href']));

                                    $format_decorators = array_filter($format_decorators, 'strlen');

                                    $filename = end($format_decorators);
                                    $fname = prev($format_decorators);*/

                                    $scenario_paths =  $this->formatPath($decorators['@attributes']['href']);

                                    $result['decorators'][] =  "$dir/$folder/" . $scenario_paths;

                                }
                            }

                            if (array_key_exists('model',$array))
                            {
                                $model = $array['model']['@attributes']['href'];

                                $model =  $this->formatPath($model);

                                $model_file_path = "$dir/$folder/" .  $model;  

                                $data = simplexml_load_file($model_file_path);

                                $parameters = json_decode(json_encode((array)$data), TRUE);

                                $paramter_paths =  $this->formatPath($parameters['nodeDecorators']['@attributes']['href']);

                                $result['parameters'][] =  "$dir/$folder/" . $paramter_paths;

                            }

                         }

                    }

                }

            }
         
        }

        return $result;   
    }

    public function updateXmlDecorators($files, $scenario)
    {
        foreach ($files['decorators'] as $decorator_path)
        {   
            if (File::exists(public_path($decorator_path)))
            {
                $decorator = simplexml_load_file(public_path($decorator_path));

                $targetISOKey = (string) $decorator->attributes()->targetISOKey;

                

                $stat = Statistics::where('scenario_id', $scenario->id)->where('targetISOKey', $targetISOKey)->where('type', 'decorator');

                if ($stat->count())
                {
                    $decorator->attributes()->infectiousCount = $stat->first()->stat_value;
                }

                
               $res =  $decorator->asXML(public_path($decorator_path));

            }
        }


        return $this;        
    }

    public function updateXmlParameters($files, $scenario)
    {
        foreach ($files['parameters'] as $params_path)
        {   


            if (File::exists(public_path($params_path)))
            {
                $param = simplexml_load_file(public_path($params_path));

                $stats = Statistics::where('scenario_id', $scenario->id)->where('type', 'parameter');

                if ($stats->count())
                {

                    foreach($stats->pluck('stat_value', 'stat_key') as $key => $stat)
                    {
                        $param->attributes()->$key = $stat;

                    }
                }

                
                $param->asXML(public_path($params_path));
            }
        }


        return $this;        
    }

    public function zip($directory, $destination)
    {
        if (file_exists(public_path($directory))) {
               

            $zip_files = glob(public_path($directory . '/*'));

            $zipFile = Zip::create(public_path($destination));

            foreach($zip_files as $zip)
            {
                
                $zipFile->add($zip);
            }

            $zipFile->close();


        } 

        return $this;

    }

    private function formatPath($text)
    {
        $format_decorators = explode('/', str_replace('#', '',$text));

        $format_decorators = array_filter($format_decorators, 'strlen');

        $filename = end($format_decorators);
        $folder = prev($format_decorators);

        return $folder . '/' .  $filename;

    }


}
