<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diseases extends Model
{
    protected $table = 'diseases';
    protected $primaryKey = 'id';
    protected $fillable = ['disease_name', 'Region', 'Province', 'Barangay', 'DAdmit', 'DOnset', 'Outcome', 'MorbidityWeek', 'Muncity', 'mdb_file_id', 'AgeYears', 'Sex', 'Classification'];
    protected $appends = ['total', 'death_percentage']; 

    public function scopeFilter($query, $request)
    {

        if ($request->has('region')) {
        	$query->where('Region', $request->input('region'));;
    	}

    	if ($request->has('province')) {
        	$query->where('Province', strtoupper($request->input('province')));
    	}

    	if ($request->has('barangay')) {
        	$query->where('Barangay', strtoupper($request->input('barangay')));
    	}

    	if ($request->has('city')) {
        	$query->where('Muncity', strtoupper($request->input('city')));
    	}

    	if ($request->has('morbidity_week')) {
        	$query->where('MorbidityWeek', $request->input('morbidity_week'));
    	}


    	return $query;
        
    }

    public function getTotalAttribute()
    {
    	return $this->alive + $this->dead;
    }

    public function getDeathPercentageAttribute()
    {
        if ($this->dead && $this->total)
        {
            $percentage =  $this->dead / $this->total;


            return number_format($percentage, 2);

        }
    	
    }
}
