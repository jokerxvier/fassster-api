<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $table = 'user_information';
    protected $primaryKey = 'id';
    protected $fillable = ['address', 'municipality', 'province', 'region', 'company'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'user_id'];
    protected $appends = ['regions'];


    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'region');
    }   

    public function getRegionsAttribute()
    {
        return $this->region()->first();
    }
}
