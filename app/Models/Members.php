<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $table = 'members';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'project_id', 'scenario_id', 'type'];
   

 
    public function members()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Projects', 'project_id');
    }
}
