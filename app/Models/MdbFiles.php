<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MdbFiles extends Model
{
    protected $table = 'mdb_files';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'sickness_id', 'user_id'];
    protected $appends = ['sickness_info'];
    protected $hidden = ['sickness_id'];

    protected static function boot() {
        parent::boot();

        static::deleting(function($mdb) { // before delete() method call this
             $mdb->diseases()->delete();
             // do the rest of the cleanup...
        });
    }

    public function sickness()
    {
        return $this->belongsTo('App\Models\Sickness', 'sickness_id');
    }

    public function diseases()
    {
        return $this->hasMany('App\Models\Diseases', 'mdb_file_id');
    }

    public function getSicknessInfoAttribute()
    {
    	return $this->sickness()->first();
    }
}
