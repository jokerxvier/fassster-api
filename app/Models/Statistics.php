<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'statistics';
    protected $primaryKey = 'id';
    protected $hidden = ['deleted_at', 'scenario_id'];
    protected $fillable = ['stat_key', 'stat_value', 'type', 'scenario_id', 'created_at', 'updated_at', 'deleted_at', 'created', 'targetISOKey'];

    public function scenario()
    {
        return $this->belongsTo('App\Models\Scenarios', 'scenario_id');
    }
}
