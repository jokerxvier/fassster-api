<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Auth;


class Projects extends Model
{
    use SoftDeletes;

    protected $table = 'projects';
    protected $primaryKey = 'id';
    protected $appends = ['owner', 'members', 'ownership_type'];
    protected $hidden = ['deleted_at', 'zip_path'];
    protected $fillable = ['title', 'description', 'image', 'zip_path', 'disease_name', 'created'];
    public static $default_img  = 'default.jpg';
    


    public static $rules = [
        'title' => 'required|max:255',
        'description' => 'required|max:500',
       	'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ];

    public function scenarios()
    {
        return $this->hasMany('App\Models\Scenarios', 'project_id');
    }

    public function owner()
    {
        return $this->belongsToMany('App\User', 'members', 'project_id', 'user_id')
                    ->where('type', 'owner')
                    ->withTimestamps();
    }

    public function user_member()
    {
        return $this->hasMany('App\Models\Members', 'project_id');
    }


    public function members()
    {
        return $this->belongsToMany('App\User', 'members', 'project_id', 'user_id')
                    ->where('type', 'member')
                    ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'members', 'project_id', 'user_id');
    }

    public function getOwnerAttribute()
    {
    	return $this->owner()->count() > 0 ?   $this->owner()->first()->name : '';
    }

    public function getOwnershipTypeAttribute()
    {
        $current = Auth::user()->id;
        $membership = $this->user_member()->where('user_id', $current);
        return $membership->count() > 0 ? $membership->first()->type  : '';
    }

    public function getMembersAttribute()
    {
    	return $this->members()
                ->get(['fname', 'lname', 'users.id', 'type'])
                ->makeHidden(['additional_information'])->toArray();
    }

    public function getImageAttribute($image)
    {
    	return Config('fassster.image_src') . '/' . $image;

    }

    public function scopeSearch($query, $search)
    {
        // Split each Name by Spaces
        $searchs = $search;

        // Search each Name Field for any specified Name
        return Projects::where(function($query) use ($searchs) {
            $query->where('title', 'like' , "%{$searchs}%");
            $query->orWhere(function($query) use ($searchs) {
                 $query->where('description', 'like' , "%{$searchs}%");
            });
        });
    }
}
