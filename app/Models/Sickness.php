<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sickness extends Model
{
    protected $table = 'sickness';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
}
