<?php

namespace App;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Hash;

class User extends Authenticatable 
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'email', 'password',
    ];




    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at', 'created_at', 'updated_at', 'pivot'
    ];

    public static $rules = [
        'fname' => 'required|max:255',
        'lname' => 'required|max:255',
        'email' => 'required|unique:users|email',
        'password' => 'required|confirmed|min:4|max:255',
        'roles' => 'required|exists:roles,id',
        'region' => 'required|exists:region,id', 
        'municipality' => 'required|max:255', 
        'address' => 'required|max:255', 
        'province' => 'required|max:255', 

    ];

    protected $appends = ['additional_information', 'name', 'user_role'];

    public static $adminRole  = 'SuperAdministrator'; 
    public static $superAdminRole  = 'SuperSuperAdministrator'; 


    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] =  Hash::make($password);
    }

    public function information()
    {
        return $this->hasOne('App\Models\UserInformation', 'user_id');
    }

    public function getUserRoleAttribute()
    {
        return ($this->roles()->count() > 0) ? $this->roles()->first()->name : false;
    }


    public function getAdditionalInformationAttribute()
    {
        return $this->information()->first();
    }

    public function getNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function scopeExceptAdmin($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->whereNotIn('name', ['SuperSuperAdministrator', 'SuperAdministrator']);
        });
    }

    public function scopeSearch($query, $search)
    {
        // Split each Name by Spaces
        $searchs = $search;

        // Search each Name Field for any specified Name
        return User::where(function($query) use ($searchs) {
            $query->where('fname', 'like' , "%{$searchs}%");
            $query->orWhere(function($query) use ($searchs) {
                 $query->where('lname', 'like' , "%{$searchs}%");
            });
        });
    }
}
