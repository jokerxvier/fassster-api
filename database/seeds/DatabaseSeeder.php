<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        //$this->call(ProjectsTableSeeder::class);
        //$this->call(ScenarioTableSeeder::class);
        //$this->call(StatisticTableSeeder::class);
        //$this->call(SimulationTableSeeder::class);
        $this->call(SicknessTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        
    }
}
