<?php

use Illuminate\Database\Seeder;

class SicknessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Sickness::class)->create([
		    'name' => 'Typhoid',
		]);

		factory(App\Models\Sickness::class)->create([
		    'name' => 'Dengue',
		]);

		factory(App\Models\Sickness::class)->create([
		    'name' => 'Measles',
		]);
    }
}
