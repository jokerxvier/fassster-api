<?php
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //User for Health Administrator
        factory(App\User::class, 1)
            ->create(['email' => 'admin@admin.com'])
            ->each(function(App\User $user) {
                factory(App\Models\UserInformation::class, 1)
                    ->create([
                        'user_id' => $user->id,
                ]);


                //$role = Role::find(1);
                //$user->assignRole($role);

                $user->assignRole('SuperSuperAdministrator');

        	});

        for ($i = 1; $i < 17; $i++)
        {
             //User for Health Administrator
            factory(App\User::class, 1)->create(['email' => 'region_'.  $i . '_admin@admin.com'])
                ->each(function(App\User $user) use($i) {
                    factory(App\Models\UserInformation::class, 1)
                        ->create([
                            'user_id' => $user->id,
                            'region' => $i
                    ]);

                    $user->assignRole('SuperAdministrator');

                });

             //User for Health Administrator
            factory(App\User::class, 2)
                ->create()
                ->each(function(App\User $user)  use($i) {
                    factory(App\Models\UserInformation::class, 1)
                        ->create([
                            'user_id' => $user->id,
                            'region' => $i
                        ]);

                    //$role = Role::find(2);
                    //$user->assignRole($role);

                    $user->assignRole('HealthAdministrator');
                });

            //User for Simulation Manager
            factory(App\User::class, 2)
                ->create()
                ->each(function(App\User $user) use($i) {
                    factory(App\Models\UserInformation::class, 1)
                        ->create([
                            'user_id' => $user->id,
                            'region' => $i
                    	]);

                    //$role = Role::find(2);
                    //$user->assignRole($role);

                    $user->assignRole('SimulationManager');
            	});

            //User for Disease Modeler
            factory(App\User::class, 2)
                ->create()
                ->each(function(App\User $user) use($i) {
                    factory(App\Models\UserInformation::class, 1)
                        ->create([
                            'user_id' => $user->id,
                            'region' => $i
                    	]);

                    //$role = Role::find(3);
                    //$user->assignRole($role);

                    $user->assignRole('DiseaseModeler');
            	});
        }




    }
}
