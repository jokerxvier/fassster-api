<?php
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Spatie\Permission\Models\Role::class)->create([
		    'name' => 'SuperSuperAdministrator',
		]);

    	factory(Spatie\Permission\Models\Role::class)->create([
		    'name' => 'SuperAdministrator',
		]);

    	factory(Spatie\Permission\Models\Role::class)->create([
		    'name' => 'HealthAdministrator',
		]);


		factory(Spatie\Permission\Models\Role::class)->create([
		    'name' => 'SimulationManager',
		]);

		factory(Spatie\Permission\Models\Role::class)->create([
		    'name' => 'DiseaseModeler',
		]);


        /*$HealthAdminRole = Role::create([
		    'name' => 'Health Administrator',
		    'slug' => 'health.admin',
		    'description' => '', // optional
		    'level' => 1, // optional, set to 1 by default
		]);

		$SimulationManagerRole = Role::create([
		    'name' => 'Simulation Manager',
		    'slug' => 'simulation.manager',
		    'description' => '', // optional
		    'level' => 1, // optional, set to 1 by default
		]);

		$DiseaseModelerRole = Role::create([
		    'name' => 'Disease Modeler',
		    'slug' => 'disease.modeler',
		    'description' => '', // optional
		    'level' => 1, // optional, set to 1 by default
		]);*/
    }
}
