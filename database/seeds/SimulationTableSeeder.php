<?php

use Illuminate\Database\Seeder;
use App\Models\Simulations;

class SimulationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json_file = public_path('simulation.json');

    	$json = file_get_contents($json_file);

    	$result = json_decode($json, true);

        Simulations::insertSimulation($result['data']);
    }
}
