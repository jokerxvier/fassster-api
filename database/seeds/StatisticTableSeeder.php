<?php

use Illuminate\Database\Seeder;
use App\Models\Scenarios;

class StatisticTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$parameters = [
    		'incubationRate',
    		'recoveryRate',
    		'transmissionRateC',
    		'immunityLossRate',
    		'infectiousToCarrierProgr',
    		'infectiousMortalityRate',
    		'transmissionRate'
 
    	];


    	$decorators = [
    		'PANDAN',
    		'DAO',
    		'GUIMBAL',
    		'CABATUAN',
    		'BINALBAGAN',
    		'TAPAZ',
    		'BAROTACVIEJO',
    		'CALATRAVA',
    		'SALVADORBENEDICTO',
    		'ISABELA',
    		'KALIBO',
    		'SARA',
    		'LAMBUNAO',
    		'JANIUAY',
    		'CULASI',
    		'JORDAN',
    		'ROXASCITY',
    		'CADIZCITY'
    	];

    

        $scenarios = Scenarios::all();

        foreach ($scenarios as $scenario)
        {

        	foreach ($parameters as $params)
        	{

        		factory(App\Models\Statistics::class, 1)
           		 ->create(['stat_key' => $params, 'type' => 'parameter', 'scenario_id' => $scenario->id]);
        	}

        	foreach ($decorators as $decorator)
        	{

        		factory(App\Models\Statistics::class, 1)
           		 ->create(['stat_key' => $decorator, 'type' => 'decorator', 'scenario_id' => $scenario->id]);
        	}

        }
    }
}
