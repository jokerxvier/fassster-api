<?php

use Illuminate\Database\Seeder;
use App\Models\Projects;
use Faker\Factory as Faker;
use App\User;

class ScenarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $projects = Projects::all();

        foreach ($projects as $project)
        {
        	factory(App\Models\Scenarios::class, 1)
            	->create(['project_id' => $project->id, 'title' => $project->title])

             ->each(function(App\Models\Scenarios $scenario) use($project) {

             	 	$faker = Faker::create();
	                $users = User::all()->pluck('id')->toArray();

	                $owner = $project->owner()->first()->id;

	                $members = array_filter($users, function($key) use ($owner) {
	                     return ($key != $owner);
	                });

	                factory(App\Models\Members::class, 1)
	                    ->create([
	            
	                        'user_id' => $owner,
	                        'scenario_id' => $scenario->id,
	                        'type' => 'owner'
	                ]);

	                factory(App\Models\Members::class, 1)
	                    ->create([
	                        'scenario_id' => $project->id,
	                        'user_id' => $faker->randomElement($members),
	                        'type' => 'member'
	                ]);
             });

             factory(App\Models\Scenarios::class, 5)
            	->create(['project_id' => $project->id, 'type' => 'sub'])

             ->each(function(App\Models\Scenarios $scenario) use($project) {

             	 	$faker = Faker::create();
	                $users = User::all()->pluck('id')->toArray();

	                $owner = $project->owner()->first()->id;

	                $members = array_filter($users, function($key) use ($owner) {
	                     return ($key != $owner);
	                });


	                factory(App\Models\Members::class, 1)
	                    ->create([
	                        'scenario_id' => $project->id,
	                        'user_id' => $faker->randomElement($members),
	                        'type' => 'member'
	                ]);
             });


             factory(App\Models\Scenarios::class, 5)
            	->create(['project_id' => $project->id, 'type' => 'sub', 'status' => 'queued'])

             ->each(function(App\Models\Scenarios $scenario) use($project) {

             	 	$faker = Faker::create();
	                $users = User::all()->pluck('id')->toArray();

	                $owner = $project->owner()->first()->id;

	                $members = array_filter($users, function($key) use ($owner) {
	                     return ($key != $owner);
	                });


	                factory(App\Models\Members::class, 1)
	                    ->create([
	                        'scenario_id' => $project->id,
	                        'user_id' => $faker->randomElement($members),
	                        'type' => 'member'
	                ]);
             });
        }
    }
}
