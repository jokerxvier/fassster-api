<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$regions = [
    		1 => 'REGION I (ILOCOS REGION)',
    		2 => 'REGION II (CAGAYAN VALLEY)',
    		3 => 'REGION III (CENTRAL LUZON)',
    		4 => 'REGION IV (CALABARZON)',
    		5 => 'REGION V (BICOL REGION)',
    		6 => 'REGION VI (WESTERN VISAYAS)',
    		7 => 'REGION VII (CENTRAL VISAYAS)',
    		8 => 'REGION VIII (EASTERN VISAYAS)',
    		9 => 'REGION IX (ZAMBOANGA PENINSULA)',
    		10 => 'REGION X (NORTHERN MINDANAO)',
    		11 => 'REGION XI (DAVAO REGION)',
    		12 => 'REGION XII (SOCCSKSARGEN)',
    		13 => 'NATIONAL CAPITAL REGION (NCR)',
    		14 => 'CORDILLERA ADMINISTRATIVE REGION (CAR)',
    		15 => 'AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)',
    		16 => 'REGION XIII (Caraga)'

    	];

    	foreach($regions as $key => $value)
    	{
	        DB::table('region')->insert([
	            'name' => $value,
	            'code' => $key,
	        ]);
	    }
    }
}


