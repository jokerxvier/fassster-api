<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Projects::class, 10)
            ->create()
            ->each(function(App\Models\Projects $project) {

                $faker = Faker::create();
                $users = User::all()->pluck('id')->toArray();

                $owner = $faker->randomElement($users);

                $members = array_filter($users, function($key) use ($owner) {
                     return ($key != $owner);
                });

                factory(App\Models\Members::class, 1)
                    ->create([
                        'project_id' => $project->id,
                        'user_id' => $owner
                ]);

                factory(App\Models\Members::class, 1)
                    ->create([
                        'project_id' => $project->id,
                        'user_id' => $faker->randomElement($members),
                        'type' => 'member'
                ]);

        	});
    }
}
