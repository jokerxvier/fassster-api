<?php

use Faker\Generator as Faker;
use Carbon\Carbon;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
    	'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => 'test', // secret
        'remember_token' => str_random(10),
        'is_active' => true
    ];
});


$factory->define(App\Models\UserInformation::class, function (Faker $faker) {
    return [
    	'address' => $faker->address,
        'municipality' => $faker->streetName,
        'province' => $faker->city,
        'region' => 5, // secret
        'company' => $faker->company,
        'user_id' => '',
    ];
});


$factory->define(Spatie\Permission\Models\Role::class, function (Faker $faker) {
    return [
    	'name' => 'Health Administrator',
    ];
});


$factory->define(App\Models\Projects::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'image' => 'default.jpg',
        'description' => $faker->text, // optional
        'disease_name' => $faker->sentence,
        'zip_path' => 'projects/zip/1_1519067318.zip',
        'created' => Carbon::createFromFormat('Y-m-d', '2016-05-13')->toDateTimeString()
    ];
});

 
$factory->define(App\Models\Members::class, function (Faker $faker) {
    return [
        'user_id' => '',
        'project_id' => NULL,
        'scenario_id' => NULL,
        'type' => 'owner', // optional
    ];
});


$factory->define(App\Models\Scenarios::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence,
        'description' => $faker->text,
        'type' => 'main', // optional
        'project_id' => 1,
        'zip_path' => 'projects/zip/1_1519067318.zip',
        'created' => Carbon::createFromFormat('Y-m-d', '2016-05-13')->toDateTimeString()
    ];
});


$factory->define(App\Models\Statistics::class, function (Faker $faker) {

    return [
        'stat_key' => $faker->word,
        'stat_value' => $faker->randomFloat(2, 0, 5),
        'type' => null, // optional
        'scenario_id' => 1
    ];
});

$factory->define(App\Models\Sickness::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
    ];
});
