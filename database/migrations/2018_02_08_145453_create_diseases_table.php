<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diseases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('disease_name')->nullable();
            $table->string('Region')->nullable();
            $table->string('Province')->nullable();
            $table->string('Muncity')->nullable();
            $table->string('Barangay')->nullable();
            $table->string('Sex')->nullable();
            $table->float('AgeYears',  8, 2)->nullable();
            $table->string('Classification')->nullable();
            $table->string('DAdmit')->nullable();
            $table->string('DOnset')->nullable();
            $table->string('Outcome')->nullable();
            $table->string('MorbidityWeek')->nullable();
            $table->integer('mdb_file_id')->nullable()->unasigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void     */
    public function down()
    {
        Schema::dropIfExists('diseases');
    }
}
