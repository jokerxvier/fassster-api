<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/', function () {

	return 'API FOR JSON';
});



Route::group(['prefix' => 'user', 'namespace' => 'Api\User'], function() {

	Route::post('register', 'RegisterController@register');
	Route::post('login', 'LoginController@login');
	Route::get('profile', 'ProfileController@index')->middleware('jwt.auth');
	Route::post('profile', 'ProfileController@update')->middleware('jwt.auth');
	Route::post('token-refresh', 'LoginController@refresh_token');
	Route::post('activate', 'ProfileController@activate_user')->middleware('jwt.auth');
	Route::post('deactivate', 'UserController@deactivate')->middleware('jwt.auth');
	Route::get('active', 'UserController@active')->middleware('jwt.auth');
	Route::get('pending', 'UserController@pending')->middleware('jwt.auth');
});

Route::group(['prefix' => 'roles', 'namespace' => 'Api\Roles'], function() {
	Route::get('/', 'RoleController@index');
});

Route::group(['prefix' => 'region', 'namespace' => 'Api\Region'], function() {
	Route::get('/', 'RegionController@index');
});


Route::group(['prefix' => 'dashboard', 'namespace' => 'Api\Dashboard'], function() {
	Route::get('/', 'DashboardController@index')->middleware('jwt.auth');
});

Route::group(['prefix' => 'projects', 'namespace' => 'Api\Projects'], function() {
	Route::get('/', 'ProjectController@index')->middleware('jwt.auth');
	Route::get('/get', 'ProjectController@getProject')->middleware('jwt.auth');
	Route::post('/upload', 'UploadController@project')->middleware('jwt.auth');
	Route::get('/details', 'ProjectController@show')->middleware('jwt.auth');
	Route::post('/edit', 'ProjectController@edit')->middleware('jwt.auth');
	Route::get('/search', 'ProjectSearchController@index')->middleware('jwt.auth');
	Route::delete('/delete', 'ProjectController@delete')->middleware('jwt.auth');
	Route::post('/share', 'ShareController@project')->middleware('jwt.auth');
	Route::delete('/member/delete', 'ShareController@remove_project_members')->middleware('jwt.auth');
	Route::get('/test', 'ProjectController@test')->middleware();
	Route::get('/test-get', 'ProjectController@getQueuedScenario')->middleware();
});

Route::group(['prefix' => 'scenarios', 'namespace' => 'Api\Scenario'], function() {
	Route::get('/', 'ScenarioController@index')->middleware('jwt.auth');
	Route::get('/get', 'ScenarioController@getScenario')->middleware('jwt.auth');
	Route::get('/getByStatus', 'ScenarioController@getByStatus')->middleware('jwt.auth');
	Route::get('/details', 'ScenarioController@show')->middleware('jwt.auth');
	Route::post('/create', 'ScenarioController@create')->middleware('jwt.auth');
	Route::post('/duplicate', 'ScenarioController@duplicate')->middleware('jwt.auth');
	Route::post('/edit', 'ScenarioController@update')->middleware('jwt.auth');
	Route::delete('/delete', 'ScenarioController@delete')->middleware('jwt.auth');
	Route::post('/share', 'ShareController@scenario')->middleware('jwt.auth');
	Route::delete('/member/delete', 'ShareController@remove_project_members')->middleware('jwt.auth');

});


Route::group(['prefix' => 'search', 'namespace' => 'Api\Search'], function() {
	Route::get('/user', 'SearchController@users')->middleware('jwt.auth');
	Route::get('/scenario', 'SearchController@scenario')->middleware('jwt.auth');
});


Route::group(['prefix' => 'mdb', 'namespace' => 'Api\Mdb'], function() {
	Route::get('/', 'MdbController@index');
	Route::post('/upload', 'MdbController@upload');
	Route::get('/reports', 'FilterController@index');
	Route::get('/reportsV2', 'FilterController@reportV2');
	Route::get('/summary_report', 'ReportController@index');
	Route::get('uploaded/list', 'MdbController@uploaded_list')->middleware('jwt.auth');
	Route::delete('uploaded/list/delete', 'MdbController@delete_mdb_files')->middleware('jwt.auth');

	Route::group(['prefix' => 'v2'], function() {
		Route::post('/upload', 'MdbController@upload_version_two')->middleware('jwt.auth');
	});

	Route::get('/sickness', 'SicknessController@index')->middleware('jwt.auth');

});





Route::group(['prefix' => 'simulation', 'namespace' => 'Api\Simulation'], function() {
	Route::get('/', 'SimulationController@index')->middleware('jwt.auth');
	Route::get('/insert', 'SimulationController@insert');
	Route::get('/regions', 'LocationController@region');
	Route::get('/provinces', 'LocationController@province');
	Route::get('/city', 'LocationController@city');
	Route::get('/graph', 'DashboardController@graph');
	Route::get('/map', 'SimulationController@map');
});
